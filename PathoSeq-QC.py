#!/usr/bin/pyhon

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
from ast import Store
# import subprocess
import os
from pickle import FALSE
# from re import S
import re
from stat import FILE_ATTRIBUTE_SPARSE_FILE
import sys
import coloredlogs, logging
import datetime
import subprocess

from modules import CovigatOff
from modules import ReadStrainScanner
from modules import Miscellaneous

# Parse the arguments
def parse_args():
    parser=argparse.ArgumentParser(description="Test python script")
    parser.add_argument("--wd", help = "Working directory", default=".", required = True)
    parser.add_argument("--fastq1", help = "Reads fastq file", required = False, default="NA")
    parser.add_argument("--fastq2", help = "Mates fastq files", required = False, default="NA")
    parser.add_argument("--fastqN", help = "Fastq File from Nanopore sequencer / Not Yet Implemented", required = False)
    parser.add_argument("--fasta", help = "Fasta Assembly file, in .fasta format", default="null")
    parser.add_argument("--nreads", help = "nreads for iss step", default = 7500000)
    parser.add_argument("--model", help = "iss model", default="NovaSeq")
    parser.add_argument("-t", "--threads", help = "Number of cpus to use", default = 2, type = int)
    parser.add_argument("-m", "--memory", help = "MAX memory to Use", default= "10g")
    parser.add_argument("--spark", help= "Trigger spark LOCAL usage for GATK. This will improve overall runtime. However, this would not create a bamout from GATK HaplotypeCaller.", action = "store_true")
    parser.add_argument("--condaBin", help = "Absolute path to conda bin directory")
    parser.add_argument("--condaEnv", help = "Absolute path to conda envs directory")
    parser.add_argument("--consensus", help = "Multifasta file with consensus sequences. SARS-CoV_2 .fasta available")
    parser.add_argument("--reference", help = "Reference Genome Fasta file path (WuhCor1 for SARS-CoV-2)")
    parser.add_argument("--host", help = "Reference Genome Fasta file path of the host", default = False)
    parser.add_argument("-n", "--name", help = "Output prefix", default = "NA")
    parser.add_argument("--minBq", help="MinBq parameter from Lofreq, and base quality for GATK", default = 20)
    parser.add_argument("--minAltBq", help="MinAltBq parameter from Lofreq", default = 20)
    parser.add_argument("--minMQ", help = "Min-mq parameter from Lofreq, and mapping quality (MQ) for GATK", default= 40)
    parser.add_argument("--minQD", help = "Min QD value for keeping variants during filtering step", default = 2)
    parser.add_argument("--maxFS", help = "Max FS value for keeping variants during filtering step", default = 60)
    parser.add_argument("--minAF", help = "Min AF value for keeping variants during filtering step & subclonal threshold for vafator annotate step", default = 0.8)
    parser.add_argument("--conservation_sarscov2", default = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionConsHMM.bed.gz" )
    parser.add_argument("--sarscov2_header",default= os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionConsHMM.header.txt")
    parser.add_argument("--sarbecovirus", default= os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionSarbecovirusConsHMM.bed.gz")
    parser.add_argument("--sarbecovirus_header", default= os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionSarbecovirusConsHMM.header.txt")
    parser.add_argument("--conservation_vertebrate")
    parser.add_argument("--conservation_vertebrate_header")
    parser.add_argument("--pfam_names")
    parser.add_argument("--pfam_names_header")
    parser.add_argument("--pfam_descriptions")
    parser.add_argument("--pfam_descriptions_header")
    parser.add_argument("--mq", help = "mq param of covigator; Mapping quality threshold", default= 1)
    parser.add_argument("--bq", help = "bq param of covigator; Base quality threshold", default= 30)
    parser.add_argument("--low_freq_threshold" , help = "low_freq_threshold for vafator annotate step", default = 0.2)
    parser.add_argument("--snpeffConfig", help = "snpeff config file path", default= os.path.dirname(os.path.realpath(__file__)) + "/reference/snpeff/snpEff.config")
    parser.add_argument("--organism", help = "target organism for snpeff step (SARS-CoV-2:Sars_cov_2.ASM985889v3.101 | H5N1:PP577941)")
    parser.add_argument("--snpeffData", help = "snpeff data directory", default= os.path.dirname(os.path.realpath(__file__)) + "/reference/snpeff/")
    parser.add_argument("--gtf", help = "Gtf reference file")
    parser.add_argument("--skip_envChecks", help = "Do not check the presence of conda environments to speed up the starting", action = "store_true")
    parser.add_argument("--skip_preprocessing", help = "Skipt GATK4 preprocessing step, default = False", action = "store_true")
    parser.add_argument("--skip_plots", help = "To skip plot generation", action = "store_true")
    parser.add_argument("--skip_ReadSS", help = "Skip the readSS step and plots", action = "store_true")
    parser.add_argument("--skip_plotCovigatOff", help = "Skip the CovigatOff step and plots", action = "store_true")
    parser.add_argument("--skip_plotUpSet", help = "Skip the UpSet plot", action = "store_true")
    parser.add_argument("--skip_snpeff", help = "Skip snpeff annotation", action = "store_true")
    parser.add_argument("--minDP", help = "Minimum Absolute DP value for variant filtering. Default= -1 -> Assume from Sample median DP and assign 5 or 100. Set to 0 to disable filtering based on DP", default = -1)
    parser.add_argument("--percDP", help = "Relative DP value used for variant filtering", default = 0)
    parser.add_argument("--zFilterThreshold", help = "Filter out variants that do not pass sd threshold during a z score analyses with respect to DP distribution of refSites", default = 0)
    parser.add_argument("--varStatsPath", help = "VarStats script path", default = os.path.dirname(os.path.realpath(__file__)) + "/modules/VarStats.R")
    parser.add_argument("--makeReport", help = "MakeReport script path", default = os.path.dirname(os.path.realpath(__file__)) + "/modules/MakeReport.0.1.R")
    parser.add_argument("--varDB", help = "Variants DB file path")
    parser.add_argument("--LabelModule", help = "Label Module to use for LABEL assignation run", default = "H5v2015")
    parser.add_argument("--virstrain_1", help = "Perform virstrain analyses using raw reads as input", action = "store_true")
    parser.add_argument("--virstrain_1_DB", help = "Path to Virstrain DB for reads analyses")
    parser.add_argument("--virstrain_3", help = "Perform two Virstrain analyses, one using PathoSeq-QC internal DB (requires fastq input) and one relying to the DB provided by the authors (which requires a fasta input; the consensus is generated by PathoSeq-QC using the variant calling data)", action = "store_true")
    parser.add_argument("--virstrain_2", help = "Perform virstrain using the author's database only. This option will use a fasta input generated after variant calling", action = "store_true")
    parser.add_argument("--virstrain_2_DB", help = "Path to Virstrain DB used with the --virstrain_2 option", default = os.path.dirname(os.path.realpath(__file__)) + "/reference/virstrain/VirStrain_contig_DB/")
    parser.add_argument("--pangolin", help = "Perform Pangolin analyses (specific for SARS-CoV-2 data)", action = "store_true")
    parser.add_argument("--freyja", help = "Perform a Freyja analysis (specific for SARS-CoV-2 data)", action = "store_true")
    parser.add_argument("--refVar", help = "Text file containing default variants to check for")
    parser.add_argument("--periscope",help = "Perform sgRNA analyses relying on the periscope tool (specific for SARS-CoV-2 data)", action = "store_true")
    parser.add_argument("--refSites", help = "Bed file with targeted regions to check for coverage support. Single bases are allowed")
    parser.add_argument("--StopAtVarCall", help = "Force the workflow to stop after the variant calling step", action = "store_true")
    parser.add_argument("-V","--version", help = "Code version", action='version', version='%(prog)s 0.1')
    parser.add_argument('-d', '--debug', help="Print all info such as debugging statements in stdout", action="store_const", dest="logLevel", const="DEBUG", default="INFO")
    parser.add_argument('-v', '--verbose', help="Pring only WARNINGS and ERRORS to stdout", action="store_const", dest="logLevel", const="WARNING")
    parser.add_argument('--quiet', help = "Do not print logs info to stdout", action = "store_false")

    parser.add_argument('--SARS_CoV_2', help="Triggers annotation of SARS-CoV-2 genome", action = "store_true")
    parser.add_argument('--H5N1', help="Triggers H5N1 analyses. It will change all reference default files accordingly.", action = "store_true")

    args=parser.parse_args()

    return args
     
                        
# Main
def main():

    inputs=parse_args()

    wd = inputs.wd
    condaBin = inputs.condaBin
    condaEnv = inputs.condaEnv
    name = inputs.name
    reads = inputs.fastq1
    mates = inputs.fastq2
    readsN = inputs.fastqN 
    assembly = inputs.fasta
    model = inputs.model 
    nreads = inputs.nreads
    threads = inputs.threads
    memory = inputs.memory
    spark = inputs.spark
    minBq = inputs.minBq
    minAltBq = inputs.minAltBq
    minMQ = inputs.minMQ
    minQD = inputs.minQD
    maxFS = inputs.maxFS
    minAF = inputs.minAF
    zFilterThreshold = inputs.zFilterThreshold
    GenomeRef = inputs.reference
    consensus = inputs.consensus
    conservation_sarscov2 = inputs.conservation_sarscov2
    sarscov2_header = inputs.sarscov2_header
    sarbecovirus = inputs.sarbecovirus
    sarbecovirus_header = inputs.sarbecovirus_header
    conservation_vertebrate = inputs.conservation_vertebrate
    conservation_vertebrate_header = inputs.conservation_vertebrate_header
    pfam_names = inputs.pfam_names
    pfam_names_header = inputs.pfam_names_header
    pfam_descriptions = inputs.pfam_descriptions
    pfam_descriptions_header = inputs.pfam_descriptions_header
    varStatsPath = inputs.varStatsPath
    mq = inputs.mq
    bq = inputs.bq
    low_freq_threshold = inputs.low_freq_threshold
    snpeffConfig = inputs.snpeffConfig
    organism = inputs.organism
    snpeffData = inputs.snpeffData
    gtf = inputs.gtf
    varDB = inputs.varDB
    makeReportPath = inputs.makeReport
    skipEnvChecks = inputs.skip_envChecks
    skipPreprocessing = inputs.skip_preprocessing
    minDP = inputs.minDP
    percDP = inputs.percDP
    skipPlots = inputs.skip_plots
    skipUpset = inputs.skip_plotUpSet
    skipReadSS = inputs.skip_ReadSS
    skipCovigatOff = inputs.skip_plotCovigatOff
    skip_snpeff = inputs.skip_snpeff
    virstrain_1 = inputs.virstrain_1
    virstrain_2 = inputs.virstrain_2
    virstrain_3 = inputs.virstrain_3
    virstrain_1_DB = inputs.virstrain_1_DB
    virstrain_2_DB = inputs.virstrain_2_DB
    pangolin = inputs.pangolin
    freyja = inputs.freyja
    refVar = inputs.refVar
    refSites = inputs.refSites
    periscope = inputs.periscope
    logLevel = inputs.logLevel
    quiet = inputs.quiet
    SARSCoV2 = inputs.SARS_CoV_2
    H5N1 = inputs.H5N1
    LabelModule = inputs.LabelModule
    host = inputs.host
    stopAtVarCall = inputs.StopAtVarCall

    if SARSCoV2:
        # Set all the reference files pointing to their respective H5N1 files.
        virstrain_1_DB = os.path.dirname(os.path.realpath(__file__)) + "/reference/virstrain/SARS_CoV_2.consensus.95percent.26.07.22.multialignment.gz"
        refVar = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/refVar.txt"
        refSites = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/refSites.bed" # ToBeCreated
        consensus = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/SARS_CoV_2.consensus.95percent.26.07.22.fasta.gz" 
        GenomeRef = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/Sars_cov_2.ASM985889v3.dna.toplevel.fa" 
        organism = "Sars_cov_2.ASM985889v3.101" # Whuan reference
        varDB = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/VarDB.26.07.22.tab" # With Respect to Wuhan reference sequence
        conservation_sarscov2  = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionConsHMM.bed.gz"
        sarscov2_header = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionConsHMM.header.txt"
        sarbecovirus = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionSarbecovirusConsHMM.bed.gz"
        sarbecovirus_header = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionSarbecovirusConsHMM.header.txt"
        conservation_vertebrate = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionVertebrateCoVConsHMM.bed.gz"
        conservation_vertebrate_header = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/wuhCor1.mutDepletionVertebrateCoVConsHMM.header.txt"
        pfam_names = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/pfam_names.bed.gz"
        pfam_names_header = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/pfam_names.header.txt"
        pfam_descriptions = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/pfam_descriptions.bed.gz"
        pfam_descriptions_header = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/pfam_descriptions.header.txt"

    if H5N1:
        # Set all the reference files pointing to their respective H5N1 files.
        virstrain_1_DB = os.path.dirname(os.path.realpath(__file__)) + "/reference/virstrain/H5N1.GenoFlu.ref.DB/" # ToBeCreated
        pangolin = False
        freyja = False
        periscope = False
        refVar = os.path.dirname(os.path.realpath(__file__)) + "/reference/H5N1/refVar.txt"
        refSites = os.path.dirname(os.path.realpath(__file__)) + "/reference/H5N1/refSites.bed"
        consensus = os.path.dirname(os.path.realpath(__file__)) + "/reference/H5N1/" # ToBeCreated
        # GenomeRef = os.path.dirname(os.path.realpath(__file__)) + "/reference/H5N1/H5N1.A.Goose.Guangdong.1.96.reference.fa" # H5N1.A.Goose.Guangdong.1.96.reference
        GenomeRef = os.path.dirname(os.path.realpath(__file__)) + "/reference/H5N1/Influenza.A.cattle.Texas.24-008749-003.2024.H5N1.fasta" # Influenza.A.Texax.37.2024.H5N1.mod.fasta" # First Human case
        # organism = "GCA_000864105.1" # H5N1.A.Goose.Guangdong.1.96.reference
        organism = "PP755589" # "PP577947" # First Human case
        varDB = os.path.dirname(os.path.realpath(__file__)) + "/reference/H5N1/VarDB.tab" # With Respect to H5N1.A.Goose.Guangdong.1.96.reference
        inputs.pangolin = pangolin
        inputs.freyja = freyja
        inputs.periscope = periscope
        
    inputs.consensus = consensus
    inputs.virstrain_1_DB = virstrain_1_DB
    inputs.refVar = refVar
    inputs.refSites = refSites
    inputs.reference = GenomeRef
    inputs.organism = organism
    inputs.varDB = varDB        

    # CHANGE ACCORDING TO INPUT TYPE - ILLUMINA or NANOPORE
    # Add here other arguments
    if name == "NA":
        if os.path.isfile(reads):
            name = os.path.basename(reads)
            name = re.sub("[_\.]1.fast.+", "", name)
            name = re.sub("[_\.]R1.fast.+", "", name)
            name = re.sub("1.fast.+", "", name)

            name = name.replace(".R1.fastq.gz", "")
            name = name.replace("_R1.fastq.gz", "")
            name = name.replace(".fastq.gz","")
            illumina = True
            nanopore = False
        elif os.path.isfile(assembly):
            name = os.path.basename(assembly)
            name = name.replace(".fasta","")
            name = name.replace(".fa","")
            illumina = True
            nanopore = False
        elif os.path.isfile(readsN):
            name = os.path.basename(readsN)
            name = name.replace("", "") # CHECK THE STANDARDS OF THE FASTQ OUTPUT NAME
            nanopore = True
            illumina = False
        inputs.name = name
    else:
        if os.path.isfile(reads):
            illumina = True
            nanopore = False
        elif os.path.isfile(assembly):
            illumina = True
            nanopore = False
        else:
            illumina = False
            nanopore = True

    ###### LOG CONFIGURATION ######
    logging.getLogger().handlers = []
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # Checking if wd is present or creating it
    if os.path.exists(wd):
        printWdWarn = 1
    else:
        printWdWarn = 0
        os.makedirs(wd)

    # Create a filehandler object
    logFile = os.path.join(wd, "PathoSeq-QC.Run." + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S" + ".log"))

    fh = logging.FileHandler(logFile)
    fh.setLevel(logging.DEBUG)
    # Create a ColoredFormatter to use as formatter for the FileHandler
    formatter = coloredlogs.ColoredFormatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    fh.setFormatter(formatter)
    logger.addHandler(fh)

    if quiet:
        # Create a stream handler for stdout
        sh = logging.StreamHandler()
        sh.setLevel(logging.DEBUG)  # Set stream handler to capture all levels
        sh.setFormatter(formatter)  # Use the same formatter for consistency
        logger.addHandler(sh)
    
        # Install colored logs for output to stdout
        coloredlogs.install(level=logLevel, logger=logger)

    if printWdWarn == 1:
        logger.warning("WD Folder %s already exists! Be aware of this...", wd)
    else:
        logger.info("Creating folder %s", wd)
    logger.info("Log file: %s", logFile)
    logger.info("The inputs are: ")
    for input in vars(inputs):
        logger.info("- {} is {}".format(input, getattr(inputs, input)))

    ####################################

    # START
    logger.info('Run started')
    # CHANGE ACCORDING TO INPUT TYPE - ILLUMINA or NANOPORE and according to the virus to be analysed
    if illumina:
        condaEnvToCheck = ["fastp","bwa","gatk","lofreq","samtools","vafator","snpeff","R"]
    else:
        condaEnvToCheck = ["fastp","minimap","","lofreq","samtools","vafator","snpeff","R"]


    if skipReadSS == False:
        condaEnvToCheck.append("bbmap")
    if virstrain_1 or virstrain_2 or virstrain_3:
        condaEnvToCheck.append("virstrain.1.17")
    if freyja:
        condaEnvToCheck.append("freyja")
    if os.path.isfile(assembly):
        condaEnvToCheck.append("iss")
    if pangolin:
        condaEnvToCheck.append("pangolin")
    if H5N1:
        condaEnvToCheck.append("LABEL")

        
    # Check the presence of the Conda environments
    if skipEnvChecks:
        logger.info("The conda environment check is currently disabled")
    else:
        for env in condaEnvToCheck:
            CovigatOff.CondaEnvCheck(condaBin,env)

    # CREATE THE FOLDER STRUCTURE #
    foldersToCreate = ["Logs","CovigatOff","Report","CovigatOff/Vcf","CovigatOff/Tabs"]

    if skipReadSS == False:
        foldersToCreate.append("ReadStrainScanner")
        foldersToCreate.append("ReadStrainScanner/Bam")
        foldersToCreate.append("ReadStrainScanner/Report")
    if assembly:
        foldersToCreate.append("Fastq")
    if virstrain_1 or virstrain_2 or virstrain_3:
        foldersToCreate.append("Virstrain")
    if freyja:
        foldersToCreate.append("Freyja")
    if pangolin:
        foldersToCreate.append("CovigatOff/Pangolin")
    if periscope:
        foldersToCreate.append("Periscope")
    if H5N1:
        foldersToCreate.append("LABEL")
        foldersToCreate.append("GenoFlu")

                        
    for items in foldersToCreate:
        path = os.path.join(wd, items)
        if os.path.exists(path):
            logger.warning("Folder %s already exists! Be aware of this...", path)
        else:
            logger.info("Creating folder %s", path)
            os.makedirs(path)

    logDir = os.path.join(wd,"Logs/")
    fastqDir = os.path.join(wd, "Fastq/")
    readSSDir = os.path.join(wd, "ReadStrainScanner/")
    FastpDir = os.path.join(wd, "CovigatOff/")
    MappingDir = os.path.join(wd, "CovigatOff/")
    BamDir = os.path.join(wd, "CovigatOff/")
    CovDir = os.path.join(wd, "CovigatOff/")
    LoFreqDir = os.path.join(wd, "CovigatOff/Vcf/")
    GATKDir = os.path.join(wd, "CovigatOff/Vcf/")
    vcf2FastaDir = os.path.join(wd, "CovigatOff/")
    pangolinDir = os.path.join(wd, "CovigatOff/Pangolin/")
    LabelDir = os.path.join(wd,"LABEL/")
    GenoFluDir = os.path.join(wd,"GenoFlu/")
    varnormDir = os.path.join(wd, "CovigatOff/")
    SARSAnnDir = os.path.join(wd, "CovigatOff/")
    vafatorDir = os.path.join(wd, "CovigatOff/")
    AnnoVafatorDir = os.path.join(wd, "CovigatOff/")
    snpeffDir = os.path.join(wd, "CovigatOff/")
    reportDir = os.path.join(wd, "Report/")
    freyjaDir = os.path.join(wd, "Freyja/")
    virstrainDir = os.path.join(wd, "Virstrain/")
    PeriscopeDir = os.path.join(wd,"Periscope/")

    ###############################

    # Running InSilico Sequencing
    if os.path.isfile(reads) == False:
        assemblyName = name
        reads, mates = Miscellaneous.iss(condaBin, condaEnv, logDir, assembly, fastqDir, assemblyName, model, str(nreads), str(threads) )

    # Launching CovigatOff pipeline
    logger.info("Starting the CovigatOff pipeline")

    # Launching Fastp
    if illumina:
        fastq1, fastq2 = CovigatOff.Fastp(condaBin,condaEnv,logDir,threads,FastpDir,fastqDir,name,illumina,**{"reads":reads, "mates":mates})
        if(host):
            fastq1, fastq2 = Miscellaneous.HostRemoval(condaBin,condaEnv,logDir,fastq1,fastq2,host,threads,CovDir,fastqDir,name)
        # Launching readStrainScanner
        if skipReadSS == False:
            logger.info("Starting ReadStrainScanner")
            ReadStrainScanner.readSS(condaBin, condaEnv, logDir, fastq1, fastq2, memory, str(threads), consensus, readSSDir, name)
        # Launching Mapping
        bam = CovigatOff.mapping(condaBin,condaEnv,logDir,fastq1,fastq2,GenomeRef,str(threads),MappingDir,name)
        # Launching Bam preprocessing
        bam = CovigatOff.preprocessing(condaBin,condaEnv,logDir,bam,GenomeRef,BamDir,name,memory,spark, skipPreprocessing)
        # Performing coverage analyses
        CovigatOff.coverage(condaBin,condaEnv,logDir,bam,CovDir,name)
        # Variants calling with LoFreq
        VcfLoFreq = CovigatOff.LoFreq(condaBin,condaEnv,logDir,bam,str(threads),GenomeRef,LoFreqDir,name,str(minBq),str(minAltBq),str(minMQ))
        # Variant calling with GATK
        VcfCaller1 = CovigatOff.GATK(condaBin,condaEnv,logDir,bam,GenomeRef,str(threads),memory,GATKDir,name,str(minBq),str(minMQ),spark)
    elif nanopore:
        fastqN = CovigatOff.Fastp(condaBin,condaEnv,threads,FastpDir,name,illumina,readsN)
        pass
        # bam = CovigatOff.mappingN() -> LoFreq Mummer

    vcfs = [VcfLoFreq, VcfCaller1]

    # Performing VarNorm
    i = 1
    for vcf in vcfs:   
        vcfName = vcf.replace(".vcf.gz","")
        vcfName = os.path.basename(vcfName)
        VcfNorm = CovigatOff.varnorm(condaBin, condaEnv, logDir, vcf, GenomeRef, varnormDir, vcfName)
        VcfNorm = CovigatOff.compress(condaBin, condaEnv, VcfNorm)
        if i == 1:
            VcfNormLoFreq = VcfNorm
        else: 
            VcfNormCaller1 = VcfNorm
        i = i + 1

    # Annotation with Vafator
    vcfs = [VcfNormLoFreq, VcfNormCaller1]
    i = 1
    for vcf in vcfs:

        vcfName = vcf.replace(".normalized.vcf.gz","")
        vcfName = os.path.basename(vcfName)

        VcfVafa = CovigatOff.vafator(condaBin, condaEnv, logDir, vcf, bam, str(mq), str(bq), vafatorDir, vcfName)
        VcfVafa = CovigatOff.compress(condaBin, condaEnv, VcfVafa)
        if i == 1:
            VcfVafaLoFreq = VcfVafa
        else: 
            VcfVafaCaller1 = VcfVafa
        i = i + 1

    # Launch annotate vafator
    vcfs = [VcfVafaLoFreq, VcfVafaCaller1]
    i = 1
    for vcf in vcfs:
        vcfName = vcf.replace(".vaf.vcf.gz","")
        vcfName = os.path.basename(vcfName)

        VcfVafaAnn = CovigatOff.annotateVafator(condaBin, condaEnv, logDir, vcf, AnnoVafatorDir, vcfName, str(low_freq_threshold), str(minAF))
        VcfVafaAnn = CovigatOff.compress(condaBin, condaEnv, VcfVafaAnn)

        if i == 1:
            VcfVafaAnnLoFreq = VcfVafaAnn
        else: 
            VcfVafaAnnCaller1 = VcfVafaAnn
        i = i + 1

    # Phasing whether requested (need --gtf)
    if gtf is not None:
        if os.path.isfile(gtf): 
            vcfs = [VcfVafaAnnLoFreq, VcfVafaAnnCaller1]
            i = 1
            for vcf in vcfs:
                vcfName = vcf.replace(".vaf.annotated.vcf.gz","")
                vcfName = os.path.basename(vcfName)
                VcfPhasing = CovigatOff.phasing(os.path.dirname(os.path.realpath(__file__)) + "/bin/Phasing.py", )
                if i == 1:
                    VcfVafaAnnLoFreq = VcfPhasing
                else: 
                    VcfVafaAnnCaller1 = VcfPhasing
                i = i + 1
    # Launch snpeff
    if not skip_snpeff:
        vcfs = [VcfVafaAnnLoFreq, VcfVafaAnnCaller1]
        i = 1
        for vcf in vcfs:
            vcfName = vcf.replace(".vaf.annotated.vcf.gz","")
            vcfName = os.path.basename(vcfName)
            VcfSnp = CovigatOff.snpeff(condaBin, condaEnv, logDir, vcf, snpeffDir, vcfName, snpeffConfig, snpeffData, organism)
            VcfSnp = CovigatOff.compress(condaBin, condaEnv, VcfSnp)
            if i == 1:
                VcfSnpLoFreq = VcfSnp
            else: 
                VcfSnpCaller1 = VcfSnp
            i = i + 1

    # Annotate SARS-CoV-2 vcf files
    if SARSCoV2:
        if not skip_snpeff:
            vcfs = [VcfSnpLoFreq, VcfSnpCaller1]         
            i = 1
            for vcf in vcfs:
                vcfName = vcf.replace(".snpeff.annotated.vcf.gz","")
                logger.debug('%s', vcf)
                vcfName = os.path.basename(vcfName)
                VcfSARS = CovigatOff.AnnotateSARS(condaBin, condaEnv, logDir, vcf, SARSAnnDir, vcfName, conservation_sarscov2, sarscov2_header, sarbecovirus, sarbecovirus_header,conservation_vertebrate, conservation_vertebrate_header, pfam_names, pfam_names_header, pfam_descriptions, pfam_descriptions_header)
                VcfSARS = CovigatOff.compress(condaBin, condaEnv, VcfSARS)
                if i == 1:
                    VcfSnpLoFreq = VcfSARS
                else: 
                    VcfSnpCaller1 = VcfSARS
                i = i + 1
        else:
            vcfs = [VcfVafaAnnLoFreq, VcfVafaAnnCaller1]
            i = 1
            for vcf in vcfs:
                vcfName = vcf.replace(".vaf.annotated.vcf.gz","")
                logger.debug('%s', vcf)
                vcfName = os.path.basename(vcfName)
                VcfSARS = CovigatOff.AnnotateSARS(condaBin, condaEnv, logDir, vcf, SARSAnnDir, vcfName, conservation_sarscov2, sarscov2_header, sarbecovirus, sarbecovirus_header,conservation_vertebrate, conservation_vertebrate_header, pfam_names, pfam_names_header, pfam_descriptions, pfam_descriptions_header)
                VcfSARS = CovigatOff.compress(condaBin, condaEnv, VcfSARS)
                if i == 1:
                    VcfSnpLoFreq = VcfSARS
                else: 
                    VcfSnpCaller1 = VcfSARS
                i = i + 1
    else:
        VcfSnpLoFreq = VcfVafaAnnLoFreq
        VcfSnpCaller1 = VcfVafaAnnCaller1

    logger.debug('Output unfiltered VCFs: %s %s', VcfSnpLoFreq, VcfSnpCaller1)

    # Make stats plots with R if requested, perform coverage analyses,
    Miscellaneous.varStats(condaBin, condaEnv, logDir, wd, reportDir, name, VcfSnpLoFreq, VcfSnpCaller1, varStatsPath, varDB, minDP, percDP, refVar, refSites, minQD, minMQ, maxFS, minAF, zFilterThreshold)

    VcfFiltLoFreq = os.path.join(reportDir, name + ".lofreq.filtered.vcf.gz" )
    VcfFiltCaller1 = os.path.join(reportDir, name + ".gatk.filtered.vcf.gz" )

    # Generate Fasta
    i = 1
    vcfs = [VcfFiltLoFreq, VcfFiltCaller1]
    for vcf in vcfs: 
        vcfName = vcf.replace(".filtered.vcf.gz","")
        vcfName = os.path.basename(vcfName)  
        if os.path.isfile(vcf):
            NoVar = False
            FastaFile = CovigatOff.vcf2fasta(condaBin, condaEnv, logDir, vcf, GenomeRef, vcf2FastaDir, vcfName)
            if i == 1:
                FastaLoFreq = FastaFile
            else:
                FastaCaller1 = FastaFile
        else: # If no variants PASS the Filters, the .filtered.vcf.gz is not produced, then vcf2fasta cannot work. IOW, cp the reference genome
            NoVar = True
            if i == 1:
                FastaLoFreq = vcf2FastaDir + vcfName + ".fasta"
            else:
                FastaCaller1 = vcf2FastaDir + vcfName + ".fasta"
            Command = f"cp {GenomeRef} {vcf2FastaDir}{vcfName}.fasta"
            logger.debug('No Variants with PASS - Running: %s', Command)
            check = os.system(Command)
            
            if check != 0:
                logger.error('Copy fasta: ERROR!')
                sys.exit('Exiting...')
            else:
                logger.info('Copy fasta: Completed')
        i = i + 1

    fastas = [FastaLoFreq, FastaCaller1]

    # End the pipeline gently
    if stopAtVarCall:
        logger.info('Run finished as requested')
        sys.exit("All Done!")

    if SARSCoV2:
        # Call Pangolin
        # Running Pangolin if requested
        if pangolin:
            for fasta in fastas:
                fastaName = fasta.replace(".fasta","")
                fastaName = os.path.basename(fastaName)
                CovigatOff.pangolin(condaBin, condaEnv, logDir, fasta, str(threads), pangolinDir, fastaName)
        # Call Freyja
        # Running Freyja if requested
        if freyja:
            Miscellaneous.freyja(condaBin,condaEnv,logDir,bam,freyjaDir,name,GenomeRef)  
            # Launch Periscope
        if periscope:
            logger.info("Running Periscope...")
            Miscellaneous.split(condaBin, condaEnv, logDir, bam, name, str(threads), PeriscopeDir)
        
            # Next variable should be put as command options (Next update)  
            script = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/periscope/search_for_sgRNA_illumina.py"
            bed = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/periscope/orf_start.bed"
            primerBed = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/periscope/artic_primers_V1.bed"
            ampliconBed = os.path.dirname(os.path.realpath(__file__)) + "/reference/SARS_CoV_2/periscope/artic_amplicons_V1.bed"

            Miscellaneous.periscope(condaBin, condaEnv, logDir, script, bam, str(50), name, bed, primerBed, ampliconBed, PeriscopeDir, str(threads), PeriscopeDir)          

    if H5N1:
        # Call LABEL
        for fasta in fastas:
            fastaName = fasta.replace(".fasta","")
            fastaName = os.path.basename(fastaName)
            Miscellaneous.LABEL(condaBin, condaEnv, logDir, str(threads), fasta, LabelDir, fastaName, LabelModule)

            Miscellaneous.GenoFlu(condaBin,condaEnv, logDir, fasta, GenoFluDir, fastaName)
    
    # Call Virstrain
    # Running Virstrain if requested
    virstrain_internalDB = False
    if virstrain_1 or virstrain_3:
        virstrain_internalDB = True
        for fasta in fastas:
            fastaName = fasta.replace(".fasta","")
            fastaName = os.path.basename(fastaName)
            # Miscellaneous.virstrain(condaBin, condaEnv, reads, mates, virstrainDir, fastaName, virstrainDB, virstrainDBDir)
            Miscellaneous.virstrain(condaBin, condaEnv, logDir, virstrainDir, fastaName, virstrain_1_DB, virstrain_internalDB, **{"reads":reads, "mates":mates})
            os.rename(virstrainDir + "/VirStrain_report.txt",virstrainDir + "/" + fastaName + ".VirStrainReport.txt")
    if virstrain_2 or virstrain_3:
        virstrain_internalDB = False
        for fasta in fastas:
            fastaName = fasta.replace(".fasta","")
            fastaName = os.path.basename(fastaName)
            Miscellaneous.virstrain(condaBin, condaEnv, logDir, virstrainDir, fastaName, virstrain_2_DB, virstrain_internalDB, **{"fasta":fasta})
            os.rename(virstrainDir + "/VirStrain_contig_report.txt", virstrainDir + "/" + fastaName + ".VirStrainContigReport.txt")

    # Launching MakeReport.R
    logger.info("Generating the reports...") 

    Miscellaneous.makeReport(condaBin, condaEnv, logDir, wd, reportDir, name, makeReportPath, varDB, skipPlots, skipReadSS, skipCovigatOff, skipUpset, minAF, refVar, refSites)
    
    # Clean folders
    # Miscellaneous.clean(wd)
    
    logger.info('Run finished')


if __name__ == '__main__':

    main()
