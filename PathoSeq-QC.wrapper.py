#!/usr/bin/pyhon

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
import glob
import datetime
import coloredlogs, logging
import pandas as pd

# Parse the arguments
def parse_args():
    parser=argparse.ArgumentParser(description="Test python script")
    parser.add_argument("--wd", help = "Working directory", default="./", required = True)
    parser.add_argument("--zSites", help = "Min Z score value used to filter custom regions", default = -3, type = float)
    parser.add_argument("--zVars", help = "Min Z score value used to filter custom variats", default = -3, type = float)
    parser.add_argument("--freyjaFreq", help = "Min Freyja Frequncy score to call sample as PURE", default = 0.99, type = float)
    parser.add_argument("--AFlowVar", help = "Max Percentage of variants under 90 AF score, to call sample as PURE", default = 0.0, type = float)
    parser.add_argument("--doublePUREcheck", help = "Change the logic of the Pureness filter from OR to AND", action="store_true" )
    parser.add_argument("--ChimeraThreshold", help = "TopOverlapScore threashold value to identify potential chimera", default = 5, type = float)
    parser.add_argument("--NewLineageThreshold", help = "Threshold to identify potential new lineages", default = 100, type = float)

    parser.add_argument("--out", help = "Output destitation file", default= os.path.dirname(os.path.realpath(__file__)) + "/WrapperOutput.tab")

    parser.add_argument('-d', '--debug', help="Print lots of debugging statements", action="store_const", dest="logLevel", const="DEBUG", default="INFO")

    args=parser.parse_args()

    return args
     
                        
# Main
def main():

    inputs=parse_args()

    wd = inputs.wd
    zScoreThresholdSites = inputs.zSites
    zScoreThresholdVars = inputs.zVars
    freyjaFreq =inputs.freyjaFreq
    AFlowVar = inputs.AFlowVar
    douplePUREcheck = inputs.doublePUREcheck
    ChimeraThreshold = inputs.ChimeraThreshold
    NewLineageThreshold = inputs.NewLineageThreshold

    output = os.path.join(wd, "SARS-Chi.Wrapper." + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S" + ".wrapper.out"))

    logLevel = inputs.logLevel

    # LOGGING
    logger = logging.getLogger(__name__)

    # Create a filehandler object
    logFile = os.path.join(wd, "SARS-Chi.Wrapper." + datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S" + ".log"))

    fh = logging.FileHandler(logFile)
    fh.setLevel(logging.DEBUG)

    # Create a ColoredFormatter to use as formatter for the FileHandler
    formatter = coloredlogs.ColoredFormatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    coloredlogs.install(level=logLevel)

    logger.info("Log file: %s", logFile)
    logger.info("The inputs are: ")
    for input in vars(inputs):
        logger.info("- {} is {}".format(input, getattr(inputs, input)))
    # ----

    # 0 Understand where to search
    SampleList = [ item for item in os.listdir(wd) if os.path.isdir(os.path.join(wd, item)) ]
    
    f = open(output, 'w')
    f.write("SAMPLE\tCHECK_SITES\tCHECK_VARS\tCHECK_PURE\tCHECK_LINEAGE(Pangolin|Virstrain|Freyja|Custom)\tOUTCOME")
    f.close()

    for Sample in SampleList:
        
        logger.info("Processing %s", Sample)

        # 1 Understand what to search
        reportFile = os.path.join(wd,Sample,"Report",Sample + ".Report.tab")
        siteFile = os.path.join(wd,Sample,"Report",Sample + ".ReportSiteCov.txt")
        varFile = os.path.join(wd,Sample,"Report",Sample + ".ReportVarCov.txt")

        if os.path.isfile(reportFile) and os.path.isfile(siteFile) and os.path.isfile(varFile):
            pass
        else:
            f = open(output, 'a')
            f.write("\n" + Sample + "\tNA\tNA\tNA\tNA\tNA")
            f.close()
            continue
        

        # 2 Cov Sites
        CovSites = pd.read_table(siteFile)
        CovSites = CovSites[CovSites.zScore <= zScoreThresholdSites]
        if (len(CovSites.index) >= 1): 
            CovSitesState = "NotPass"
        else:
            CovSitesState = "Pass"

        # 3 Cov Vars
        CovVars = pd.read_table(varFile)
        CovVars = CovVars[CovVars.zScore <= zScoreThresholdVars]
        if (len(CovVars.index) >= 1):
            CovVarsState = "NotPass"
        else:
            CovVarsState = "Pass"
    
        # 4 Pure
        Report = pd.read_table(reportFile)

        if Report.at[0,'GATK.VarPercBelow.AF.90'] != 'No GATK data':

            if douplePUREcheck:
                if (Report.at[0,'GATK.VarPercBelow.AF.90'] <= AFlowVar) & (Report.at[0,'FreyjaTopLinFreq'] >= freyjaFreq):
                    PureCheck = "Pass"
                else:
                    PureCheck = "NotPass"
            else:
                if (Report.at[0,'GATK.VarPercBelow.AF.90'] <= AFlowVar) | (Report.at[0,'FreyjaTopLinFreq'] >= freyjaFreq):
                    PureCheck = "Pass"
                else:
                    PureCheck = "NotPass"
        else:
            PureCheck = "NoVarData"

        # 5 Lineages
        Lineages = str(Report.at[0,'Pangolin.out']) + "|" + str(Report.at[0,'Virstrain.out']) + "|" + str(Report.at[0,"FreyjaTopLin"]) + "|" + str(Report.at[0,"TopOverlap"])

        # 6 Chimera
        if PureCheck == "Pass":
            if Report.at[0,'TopOverlapScore'] < ChimeraThreshold & ((Report.at[0,'TopOverlapSampleFreq'] + Report.at[0,'TopOverlapLineageFreq']) < NewLineageThreshold):
                Outcome = "PotentialNewLineage"
            elif Report.at[0,'TopOverlapScore'] >= ChimeraThreshold:
                Outcome = "PotentialChimera"
            else:
                Outcome = "KnownLineage" 
        elif PureCheck == "NoVarData":
            Outcome = "NoVarData - WhuCor1"
        else:
            Outcome = "PotentialMixture"

        # 7 Create the output
        f = open(output, 'a')
        f.write("\n" + Sample + "\t" + CovSitesState + "\t" + CovVarsState + "\t" + PureCheck + "\t" + Lineages + "\t" + Outcome)
        f.close()

    f = open(output, 'a')
    f.write("\n")
    f.close()

if __name__ == '__main__':

    main()
