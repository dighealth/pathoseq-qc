<div id="top"></div>


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://citnet.tech.ec.europa.eu/CITnet/stash/projects/DIGHEALTH/repos/pathoseq-qc/browse">
  </a>

  <h3 align="center">PathoSeq-QC</h3>

  <p align="center">
    A bioinformatics decision-support workflow for the analyses of pathogen's data
    
    REAMDE for V.2

  </p>
</div>

## Table of contents
* [What is PathoSeq-QC?](#what-is-pathoseq-qc?)
* [Pipeline description](#pipeline-description)
* [Installation](#installation)
* [How to run it](#how-to-run-it)
* [Options](#options)
* [To do list](#to-do-list)
* [Report examples](#report-examples)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgments](#acknowledgments)

## What is PathoSeq-QC?

PathoSeq-QC is a bioinformatics command-line pipeline designed to retrieve robust and trustworthy results from the analysis of pathogens' raw next-generation sequencing (NGS) data to support policymaking. It emphasizes quality and stringent filtering, employing a redundant approach for the most critical steps, such as variant calling and lineage assignment. PathoSeq-QC version 1 was developed exclusively for SARS-CoV-2. With version 2, we have expanded its capability to generate reliable data for potentially any pathogen species (tested on *A(H5N1)* and *Oropouche* viruses).
The tool includes the following features: assesment of raw reads quality, filtering of raw NGS reads, host-read filtering, mapping, coverage analysis, variant calling, filtering and annotation, lineage assignment, and assessment of sample heterogeneity. It also provides easy-to-read plots and tables compliant with the **FAIR** (Findable, Accessible, Interoperable, Reusable) principles.

<p align="right">(<a href="#top">back to top</a>)</p>

## Pipeline description

PathoSeq-QC is centered on **quality**. The first step employed by the tool is an assessment of the NGS reads data, coupled with the removal of low-quality reads (*e.g.*, reads that are too short or have low nucleotide quality). Users can also remove reads that map to a particular FASTA sequence (*e.g.*, to eliminate host-mapping reads).

This high-quality reads dataset is then used for the mapping step. PathoSeq-QC is also implemented with [GATK4 preprocessing](https://gatk.broadinstitute.org/hc/en-us/articles/360035535912-Data-pre-processing-for-variant-discovery) procedures, which are meant to remove sequencing errors and duplicated reads (potentially generated during PCR amplifications), which might insert biases in reads support during variant calling procedures.

PathoSeq-QC version 1 originally comprised three main script modules: *ReadStrainScanner*, *CovigatOff*, and *MakeReport*. With version 2, we have added a fourth module, named *VarStats*. Below is the diagram:

<p align="center">
  <img src="PathoSeq-QC.diagram.V2.jpg" width="75%">
</p>

### ReadStrainScanner (ReadSS) - DEPRECATED

This module, deprecated in version 2, mapped filtered read sequences to a set of custom-made consensus sequences. For SARS-CoV-2, these consensus sequences were generated by Mauro Petrillo (ext EU-JRC, Ispra) using only variants observed in at least 95% of GISAID sequences.

After mapping, the module calculated the frequencies of **UnAmbiguously** mapped and **Ambiguously** mapped reads per consensus sequence. This information could be used to potentially identify the most probable lineage (pathogen). This module relied on the following *bbmap* command:

```
$    bbmap.sh -Xmx$MEM threads=$THREADS ref=$CONSENSUS perfectmode=T maxsites=1 out=$MAPPED.bam outu=$UNMAPPED.BAM scafstats=$SCAFSTATS bamscript=bs.sh
```

As shown in the command line, mapped and unmapped reads were stored and could be further investigated at the end of the pipeline.

**NOTE:** This step is deprecated as it is extremely time and resource consuming.

<p align="right">(<a href="#top">back to top</a>)</p>

### CovigatOff

The CovigatOff module is an offline improvement of the [Covigator pipeline](https://github.com/TRON-Bioinformatics/covigator-ngs-pipeline). 
Briefly, this module can process mapped reads to perform variant calling, filtering, annotations, and lineage assignment using a variety of tools such as *GATK4*, *LoFreq*, *Vafator*, *Snpeff*, and *Pangolin*, in the same way as Covigator would. However, there are several major improvements over the original Covigator pipeline. We added the capability to run it offline. Once all the required conda environments are set up (see **Prerequisites** section), the tool is ready to go, allowing for offline analyses which are needed for clusters infrastructures. We also improved overall performance by applying multithreaded versions of *fastp* and *LoFreq* tools, and by adding the *spark* commands related to GATK. Please see the Covigator GitHub page for additional information. 

While the original Covigator pipeline was designed for SARS-CoV-2, this module allows the analysis of any pathogen's data, with special support for SARS-CoV-2 and a(H5N1) viruses (*i.e.* reference files).

This module leverages **redundancy** to enhance the overall trustworthiness of the results. This means that several tools with the same purpose are applied to the same inputs. For variant calling, both *GATK HaplotypeCaller* and *LoFreq* are used. These tools have different strengths and weaknesses. *HaplotypeCaller* is a state-of-the-art tool capable of retrieving robust variant data, but it lacks the ability to identify low-frequency variants (AF < 0.5). Conversely, *LoFreq* allows the detection of low-frequency variants but increases the probability of false positive calls. These data are also used to understand the heterogeneity of the sample (*i.e.*, whether multiple lineages of the same pathogen are within the same sample), which might affect the reliability of some lineage assignment approaches and can represent a proof for contaminated samples.

For lineage assignment, *PathoSeq-QC* V.2 relies on pathogen-specific and pathogen-non-specific tools such as *virstrain*, *Pangolin*, etc. Please refer to the tools' manuals for further information (see Diagram).

Additionally, this module performs coverage analyses on the mapped data (after GATK4 preprocessing steps, if selected). Distribution of DP values, at selected sites, is used to compute zscore analyses to understand whether sequencing data are homogeneous in terms of coverage.

#### SARS-CoV-2 virus Support

SARS-CoV-2 specific analyses can be triggered with the **--SARS-CoV-2** option. With this, specific annotation of the variants and the analysis of lineage composition through *Pangolin* and *Freyja* are activated. Additionally, these can be activated using their specific options.

Specifically for SARS-CoV-2, *PathoSeq-QC* has been implemented with [Periscope](https://github.com/sheffield-bioinformatics-core/periscope) (**--periscope**) to scan for the presence of sgRNA from raw Illumina data (Periscope code has been adapted for this purpose).

#### A(H5N1) virus Support

H5N1 specific analyses can be triggered with the **--H5N1** option. With this, the tool uses the **A/cattle/Texas/24-008749-003/2024(H5N1)** sequence as reference genome and performs lineage assignment using tools such as *LABEL* and *GenoFlu*. These can also be triggered independently from a H5N1 specific analyses using the options **--LABEL** and **--GenoFlu**.

<p align="right">(<a href="#top">back to top</a>)</p>

### VarStats

The new *VarStats* module merges variant calls data with coverage analyses to perform variant filtering. The resulting variants are then passed again to the *CovigatOff* module, which generates consensus sequences and performs run lineage assignment analyses. The *VarStats* module also creates easy-to-read summary plots to help users understanding the effects of filters on the final results. PathoSeq-QC can be forced to stop after the generation of these plots when only exploratory analysis on samples are required (*i.e* when there is no need to retrieve lineage data and final outputs, see the full set of options). This is extremely usefull to understand the effects of filters and to select the proper filtering thresholds values on the selected samples.

Here below, an output example from the analyses of a SARS-CoV-2 sample.

<p align="center">
  <img src="./examples/VarStats.plots.jpg" width="75%">
</p>

<p align="right">(<a href="#top">back to top</a>)</p>

### MakeReport

The MakeReport module is in charge of generating the workflow's main outputs by integrating the results from PathoSeq-QC's modules. There are currently seven outputs: 2 PDF files with plots and 7 table files. Below is a short description of the outputs. Users are encouraged to read the [PathoSeq-QC report](https://op.europa.eu/en/publication-detail/-/publication/2f4deb18-22e7-11ef-a251-01aa75ed71a1/language-en) for more detailed information. The below examples refer to the anlyses of sample **OV754178.WGS.novaseq.WGS.817x** (an *in-silico* generated *SARS-CoV-2* dataset) which can be downloaded from the following **DOI: 10.2905/f4cb0c69-5d43-4765-9ec8-e79cb28c21bc**

> **.PrivateVarVsTopUnambiguous.tab**

If the readSS module is used, this file will contains the set of variants private to the sample, with respect to the top five unambiguously mapped lineages.

> **.ReportOverlaps.txt**

This file contains the results from the overlap analyses ordered by Overlapping Score. The top result is the lineage with the highest number of variants observed within the lineage (column **SampleInLineageOutTotLineage**) and the highest number of variants observed within the sample (column **SampleInLineageOutTotSample**). Overlap analyses are made at contig/chr level (if more than one is present in the reference genome) and at the whole genome level (chrom = Whole Genome).

| lineages   | SampleInLineageOutTotSample | score_AB | SampleInLineageOutTotLineage | score_BA          | score | chrom        |
|------------|-----------------------------|----------|------------------------------|-------------------|-------|--------------|
| BA.2.23    | 96.2962962962963            | 2        | 80                           | 5                 | 7     | Whole Genome |
| BA.2.1     | 96.2962962962963            | 2        | 78.7878787878788             | 7                 | 9     | Whole Genome |
| BA.2.62    | 94.4444444444444            | 3        | 78.4615384615385             | 8                 | 11    | Whole Genome |
| BA.2.2     | 96.2962962962963            | 2        | 76.4705882352941             | 10                | 12    | Whole Genome |
| BA.2.11    | 83.3333333333333            | 9        | 78.9473684210526             | 6                 | 15    | Whole Genome |
| BA.2.18    | 94.4444444444444            | 3        | 76.1194029850746             | 12                | 15    | Whole Genome |
| BA.2.3.16  | 94.4444444444444            | 3        | 76.1194029850746             | 12                | 15    | Whole Genome |
| BA.2.37    | 94.4444444444444            | 3        | 76.1194029850746             | 12                | 15    | Whole Genome |
| BA.2.41    | 94.4444444444444            | 3        | 76.1194029850746             | 12                | 15    | Whole Genome |


> **.Report.Plots.pdf**

This file includes:
1. Distribution of AF scores (from GATK and LoFreq) for all detected variants, which can help in understanding the heterogeneity of a sample.
2. Results of the custom lineage approach.

> **.Report.tab**

This is one of the main output of the pipeline, which summarise the major results of the analyses. It provides coverage metrics along side variants calling and filtering infomation. Lineages, as assigned by the different tools, are also reported.

| Sample                           | CoverageMean | CoverageSd   | CoverageMedian | RefSitesQualCheck | RefVarsQualCheck | GatkCallsCount | GatkPassCallsCount | GatkPassCallsPerc | GatkDPPassCount | GatkAFPass | GatkZscorePassCount | LoFreqCallsCount | LoFreqPassCallsCount | LoFreqPassCallsPerc | LoFreqDPPassCount | LoFreqAFPassCount | LoFreqZscorePassCount | UnknownsVariantsCount | LineagePrivateVariantsCounts | PangolinLineage | PangolinLineageScore | FreyjaLineage | FreyjaLineageFrequency | FreyjaVOC | FreyjaVOCFrequency | LABELLineage | GenoFluLineageVirstrainReadsBest | VirstrainContigBestSp       | VirstrainContigBestSt | CustomTopLineage | SampleInLineageOutTotSample | SampleInLineageOutTotLineage | CustomTopLineageScore | TopUnAmbiguousLineage | TopUnAmbiguousReadsFreq |
|----------------------------------|--------------|--------------|----------------|-------------------|------------------|----------------|--------------------|-------------------|----------------|------------|---------------------|------------------|----------------------|--------------------|-------------------|-------------------|-----------------------|------------------------|-----------------------------|-----------------|----------------------|----------------|------------------------|-----------|---------------------|--------------|-------------------------------|---------------------------|------------------------|------------------|-------------------------------|-----------------------------|--------------------|---------------------|------------------------|
| OV754178.WGS.novaseq.WGS.817x    | 752          | 142.873451177 | 807            | PASS              | PASS             | 54             | 54                 | 100               | 54             | 54         | 54                  | 56               | 56                   | 100                | 56                | 56                | 56                    |                        | BA.2                        | 0               | XAP                  | 0.99629352     | Omicron                | 0.999999999200743 |                     | SCOV2        | >hCoV-19/Jiangsu/NJ_08/2022      | BA.2.23                  |


> **.Report.UpSet.pdf**

This UpSet diagram represents the overlaps found between the sample variants and the top five unambiguously mapped lineages. The Pangolin lineage may also be present according to the Pangolin result.

> **.ReportVarData.tab**

Within this table, all variant data, including DP and AF values, statistics and filtering outcomes are reported.

| CHROM      | POS  | REF | ALT | AF    | DP  | QD    | MQ  | FS | Sample                           | Caller | Var                       | Filter | DP.zScore             | DP.zScore.PASS | OverlappingCall | DP.PASS | MQ.PASS | QD.PASS | FS.PASS | AF.PASS |
|------------|------|-----|-----|-------|-----|-------|-----|----|----------------------------------|--------|--------------------------|--------|-----------------------|----------------|-----------------|---------|---------|---------|---------|---------|
| MN908947.3 | 2790 | C   | T   | 0.999 | 812 | 25.36 | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_2790:C-T      | PASS   | 0.0463658101537277    | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 3037 | C   | T   | 1     | 838 | 28.73 | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_3037:C-T      | PASS   | 0.709720708761385     | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 4321 | C   | T   | 1     | 797 | 30.97 | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_4321:C-T      | PASS   | -0.336338939042997    | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 9344 | C   | T   | 1     | 689 | 27.24 | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_9344:C-T      | PASS   | -3.09181313325942     | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 9424 | A   | G   | 1     | 701 | 28.2  | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_9424:A-G      | PASS   | -2.78564933390204     | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 9534 | C   | T   | 1     | 629 | 25    | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_9534:C-T      | PASS   | -4.62263213004632     | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 9866 | C   | T   | 1     | 704 | 29.56 | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_9866:C-T      | PASS   | -2.70910838406269     | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 10029| C   | T   | 1     | 757 | 30.62 | 60  | 0  | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_10029:C-T     | PASS   | -1.35688493690093     | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |
| MN908947.3 | 11287| GTCTGGTTTT | G | 1 | 816 | 28.17 | 60 | 0 | OV754178.WGS.novaseq.WGS.817x    | GATK   | MN908947.3_11287:GTCTGGTTTT-G | PASS | 0.148420409939521     | PASS           | Yes             | PASS    | PASS    | PASS    | PASS    | PASS    |


> **.ReportVarDB.tab**

This table contains all the variants observed from the consensus file, along with:
1. Counts of lineages sharing the same variant
2. Indication if a variant was observed in the sample
3. Novel variants present within the sample but not within the Consensus DB

| var                    | lineage  | Chrom      | CountsOfLineagesWithVar | VarIsLineageSpecific | ObservedInSample | Pos             |
|------------------------|----------|------------|-------------------------|----------------------|------------------|-----------------|
| MN908947.3_10015:C-T   | B.1.1.524| MN908947.3 | 2                       | No                   | No               | MN908947.3_10015|
| MN908947.3_10015:C-T   | B.1.606  | MN908947.3 | 2                       | No                   | No               | MN908947.3_10015|
| MN908947.3_10029:C-T   | A.2.5    | MN908947.3 | 424                     | No                   | Yes              | MN908947.3_10029|
| MN908947.3_10029:C-T   | A.2.5.1  | MN908947.3 | 424                     | No                   | Yes              | MN908947.3_10029|
| MN908947.3_10029:C-T   | A.2.5.2  | MN908947.3 | 424                     | No                   | Yes              | MN908947.3_10029|
| MN908947.3_10029:C-T   | A.2.5.3  | MN908947.3 | 424                     | No                   | Yes              | MN908947.3_10029|
| MN908947.3_10029:C-T   | AT.1     | MN908947.3 | 424                     | No                   | Yes              | MN908947.3_10029|
| MN908947.3_10029:C-T   | AY.1     | MN908947.3 | 424                     | No                   | Yes              | MN908947.3_10029|
| MN908947.3_10029:C-T   | AY.100   | MN908947.3 | 424                     | No                   | Yes              | MN908947.3_10029|

> **.ReportSiteCov.tab**

This table contains the results of a zScore analyses performed using the DP values of selected loci. For SARS-CoV-2 these represent the Amplicon regions of several PCR detection methods, and are fully customizable by the user. The DP distribution in this region is also used to perform zScore analyses at selected variant position (see next output description). This zScore analyses is meant to provide an overview of the sequencing "goodness" as extreme DP values at very well known loci might inder the presence of some protocol/technical issue. 

| CHROM      | POS  | zScore               | DP  |
|------------|------|----------------------|-----|
| MN908947.3 | 4595 | -0.132229739471411   | 805 |
| MN908947.3 | 4596 | -0.208770689310756   | 802 |
| MN908947.3 | 4597 | -0.234284339257204   | 801 |
| MN908947.3 | 4598 | -0.081202439578514   | 807 |
| MN908947.3 | 4599 | -0.132229739471411   | 805 |
| MN908947.3 | 4600 | 0.0208521602072794   | 811 |
| MN908947.3 | 4601 | -0.0556887896320657  | 808 |
| MN908947.3 | 4602 | -0.157743389417859   | 804 |
| MN908947.3 | 4603 | -0.0301751396856173  | 809 |


> **.ReportVarCov.tab**

This table contains the results of the zscore analyses performed on the reference variant sites. To perform zScore analyses, the DP distribution at very well known sites is used. For SARS-CoV-2 these are the Amplicon region of the most widely adopted PCR detection methods (See previous table description). These results can be used to understand whether the coverage at specific variant sites was sufficiently in line with the overall DP values. Variants are completely customizable by the user.

| VAR                  | DP  | zScore              |
|----------------------|-----|---------------------|
| MN908947.3_3037:C-T  | 804 | -0.157743389417859  |
| MN908947.3_14408:C-T | 804 | -0.157743389417859  |
| MN908947.3_23403:A-G | 804 | -0.157743389417859  |


<p align="right">(<a href="#top">back to top</a>)</p>

### InSilicoSeq

For development purposes, a fifth module is present. This allows the user to set a consensus sequence as an input file without needing raw reads data. Raw reads will be generated from the consensus sequence using the [InSilicoSeq tool](https://insilicoseq.readthedocs.io/en/latest/iss/generate.html).

<p align="right">(<a href="#top">back to top</a>)</p>

## Installation

### Prerequisites

The following tools are requested. Some of them are mandatory. Others are needed only if specific options are triggered. 

- *[Python](https://www.python.org/)* (Tested with v. 3.7)
- Python Libraries: *[coloredlogs](https://coloredlogs.readthedocs.io/en/latest/)*
- [Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/index.html)*
- Conda environements:
     - NAME: **bbmap**; tools: *[bbmap](https://jgi.doe.gov/data-and-tools/software-tools/bbtools/bb-tools-user-guide/bbmap-guide/)* (v. 38.96) - Optional
     - NAME: **fastp**; tools: *[fastp](https://github.com/OpenGene/fastp)* (v. 0.22.0) - **REQUIRED**
     - NAME: **gatk**; tools: *[gatk4](https://gatk.broadinstitute.org/hc/en-us/articles/360036194592-Getting-started-with-GATK4)* (v. 4.2.6.1), *[bcftools](https://samtools.github.io/bcftools/bcftools.html)* (v. 1.15.1) - **REQUIRED**
     - NAME: **lofreq**; tools: *[lofreq](https://csb5.github.io/lofreq/)* (v. 2.1.5), *[bcftools](https://samtools.github.io/bcftools/bcftools.html)* (v. 1.15.1) - **REQUIRED**
     - NAME: **bwa**; tools: *[bwa](http://bio-bwa.sourceforge.net/)* (v. 0.7.17-r1188), *[samtools](http://www.htslib.org/)* (v. 1.15.1) - **REQUIRED**
     - NAME: **samtools**; tools: *[samtools](http://www.htslib.org/)* (v. 1.15.1) - **REQUIRED**
     - NAME: **vafator**; tools: *[vafator](https://github.com/tron-bioinformatics/vafator)* (v. 1.3.0) - **REQUIRED**
     - NAME: **snpeff**; tools: *[snpeff](http://pcingola.github.io/SnpEff/)* (v. 5.1d) - **REQUIRED**
     - NAME: **pangolin**; tools: *[pangolin](https://github.com/cov-lineages/pangolin)* (v. 4.0.6) - Optional
     - NAME: **virstrain**; tools: *[virstrain](https://github.com/liaoherui/VirStrain)* (v. 1.17) - Optional
     - NAME: **genoflu**; tools: *[genoflu](https://github.com/USDA-VS/GenoFLU)* (v. 1.03) - Optional
     - NAME: **LABEL**; tools: *[LABEL](https://wonder.cdc.gov/amd/flu/label/)* (v. 0.6.4) - Optional
     - NAME: **freyja**; tools: *[freyja](https://github.com/andersen-lab/Freyja)* (v. 1.3.8) - Optional
     - NAME: **R**; tools: *[R](https://www.r-project.org/)* (v. 4.1.3); Libraries: [data.table](https://cran.r-project.org/web/packages/data.table/vignettes/datatable-intro.html), [ggplot2](https://ggplot2.tidyverse.org/reference/ggplot.html), [patchwork](https://patchwork.data-imaginist.com/), [argparse](https://cran.r-project.org/web/packages/argparse/index.html), [UpSetR](http://gehlenborglab.org/research/projects/upsetr/), [RColorBrewer](https://rdrr.io/cran/RColorBrewer/), [stringdist](https://cran.r-project.org/web/packages/stringdist/stringdist.pdf) - **REQUIRED**

     - NAME: **iss**; tool: *[InSilicoSeq](https://insilicoseq.readthedocs.io/en/latest/iss/generate.html)* (v. 1.5.3), *[pigz](https://zlib.net/pigz/)* (v. 2.6) - Optional

<p align="right">(<a href="#top">back to top</a>)</p>

### Installation guide

#### Automatic Installation

Download and extract this repository. Then, run:

```
    cd PathoSeq-QC;
    bash INSTALL.sh /WHERE/DO/YOU/WANT/TO/CREATE/THE/CONDA/ENVIRONMENTS/PATH/;
```

This should create all the necessary conda environment with the needed dependencies. It will also install Python *coloredlogs* library and R libraries. You can now create the virstrain database (see below for further instructions).

#### Manual installation 

Please use the same names provided above. For each environement, install the requested tools/libraries using *[conda](https://docs.conda.io/en/latest/)/[mamba](https://github.com/mamba-org/mamba)* and the standard *install.packages()* R function.

Example

```
    conda create -n gatk
    conda activate gatk
    conda/mamba install -c bioconda gatk4
    conda deactivate
```

For R, please see: https://www.biostars.org/p/498049/

##### Checking python version and Installing the *coloredlogs* python library 

Example

```
    python -V
    conda install -c conda-forge coloredlogs
```
##### Changing the ISS code (if you need to use it)

*InSilicoSeq* saves all the generated reads into RAM, initially. This could result in memory problems.
A tournaround is to let *InSilicoSeq* to save all the reads directly to disk instead. The solution is reported here:  https://github.com/HadrienG/InSilicoSeq/pull/210

#### Creating the Virstrain Database

To run *Virstrain* analyes there are currently two options. 1) Perform contig-based analyses; 2) Perform reads-based analyses.
For the first case, the Virstrain custom database is already up and running. Therefore, this step can be skipped. However, to perform the reads-based analyses, the used should first: decompress the consensus database file

```
    gunzip /your/repo/reference/virstrain/consensus.95percent.09.01.22.multialignment.gz
```

Activate the Virstain environment, then launch the virstrain_build command

```
    conda activate virstrain;
    cd /your/repo/reference/virstrain
    virstrain_build -i /your/repo/reference/virstrain/consensus.95percent.09.01.22.multialignment -d /your/repo/reference/virstrain/ConsensusVirstrainDB -s 0.8
    conda deactivate
```

Please note that the following warnings are expected during this phase.

> Use of uninitialized value $char in pattern match (m//) at /your/condaEnv/path/virstrain/lib/python3.7/site-packages/VirStrain/aln2cluster-overlap-kmer-withd.pl line 81

#### Test it!

Good. Now you're ready to go.

```
    python PathoSeq-QC.py --wd /yourDir/Test/ --fastq1 /RepoDir/examples/Test_R1.fastq.gz --fastq2 /RepoDir/examples/Test_R2.fastq.gz  --condaBin /your/Conda/Bin/Dir  --condaEnv /your/Conda/Env/Dir -t 1 --skip_readSS
```

<p align="right">(<a href="#top">back to top</a>)</p>

## How to run it

PathoSeq-QC is designed to rely on paired-end reads data only.

```
python PathoSeq-QC.py [options] --fastq1 reads.fastq.gz --fastq2 mates.fastq.gz
```

For test purposes it can also use fasta consensus sequences, that can be used to generate in silico reads.

```
python PathoSeq-QC.py [options] --fasta consensus.fasta
```

### Best Practice

As a best practice, please use always absolute paths to files/directories and **use the --wd option to specify where to save all the results**.

If you already installed the conda environments, you can avoid the automatic checks at the beginning of the analyses with the --skip_envChecks. This will improve performances and the overall runtime.

Check all the options. There are options to disable some passages, which might not be needed for your case (*e.g.,* --skip_snpeff)

### HTCondor compatibility

To run it on HTCondor infrastracture, the user need to set up the PathoSeq-QC.subset.sh file. This will run the bin/PathoSeq-QC.wrapper.py file which can be populated with all the PathoSeq-QC option needed. 

<p align="right">(<a href="#top">back to top</a>)</p>

## Options

### Basic options

**--wd**: Absolute path to the working directory. A hierarchical folder structure will be generated here.

**--fastq1**: First Set of reads

**--fastq2**: Second Set of reads

**--fasta**: Fasta mode (in alternative to --fastq1 and --fastq2); this will trigger the *in-silico* generation of reads (through *InSilicoSeq*) based on the FASTA file provided, which will be used for the analyses.

**--condaBin**: Absolute path to the *Conda* bin folder

**--condaEnv**: Absolute path to the *Conda* env absolutefolder

**--SARS_CoV_2**: Triggers a SARS-CoV-2 specific analyses. It will change all reference default files accordingly **[False]**

**--H5N1**: Triggers A(H5N1) virus analyses. It will change all reference default files accordingly. PathoSeq-QC will run both *LABEL* and *GenoFlu* by default. **[False]**

**-t** | **--threads**: Number of threads to use

**-m** | **--memory**: memory limit **[20g]**. It applies to bbmap/gatk and others java steps

**--man**|**help**|**h**: print all the option information

**-V**: Current version **[2]**

**-d** | **--debug**: pring DEBUG and INFO logs

**-v** | **--verbose**: print only Warnings and Errors

**--quiet**: Do not print logs info to stdout **[False]**

### Filtering options

**--minDP**: Minimum Absolute DP value for variant filtering. By default, 5 or 100 is selected according to the magnitude of the sample median DP. Not used if percDP results in a higher DP threashold value.

**--percDP**: Minimum DP percentage threashold for variant filtering. By default selected to 10% of the sample median DP. Not used if minDP results in a higher DP threashold value.

**--minBq**: MinBq parameter from Lofreq, and base quality for GATK **[20]**

**--minAltBq**: MinAltBq parameter from Lofreq **[20]**

**--minMQ**: Min-mq parameter from Lofreq, and mapping quality (MQ) for GATK **[40]**

**--minQD**: Min QD value for keeping variants during filtering step **[2]**

**--maxFS**: Max FS value for keeping variants during filtering step **[60]**

**--minAF**: Min AF value for keeping variants during filtering step & subclonal threshold for vafator annotate step **[0.8]**

**--zFilterThreshold**: Standard Deviation value to be used to filter out variants that do not pass sd threshold during a z score analyses with respect to DP distribution of refSites. Disabled by default. **[0]**

For the full set of filtering options, please check the command line options with the following command: 

```
$ python PathoSeq-QC.py -h|--help
```

### Lineage assignation options

**--virstrain_1**: Performs virstrain analyses using raw reads as input **[False]**

**--virstrain_1_DB**: Absolute path to the Virstrain DB for reads analyses (if --virstrain_1 is used)

**--virstrain_2**: Performs virstrain using the author's database only. This option will use a fasta input generated after variant calling **[False]**

**--virstrain_2_DB**: Absolute path to the Virstrain DB used (if --virstrain_2 is used).

**--virstrain_3**: Performs two Virstrain analyses, one using PathoSeq-QC internal DB (requires fastq input) and one relying to the DB provided by virstrain authors (which requires a fasta input; the consensus is generated by PathoSeq-QC using the variant calling data) **[False]**

**--pangolin**: Performs Pangolin analyses (specific for SARS-CoV-2 data). Not triggered by default by --SARS_CoV_2. **[False]**

**--freyja**: Performs a Freyja analysis (specific for SARS-CoV-2 data). Not triggered by dafault by --SARS_CoV_2. **[False]**

**--LabelModule**: Label Module to be used for LABEL assignations **[H5v2015]**

### Additional options

**--consensus**: Multifasta with lineage reference genome sequences (defauls: set of 1500 SARS-CoV-2 consensus).

**--reference**: Reference genome sequence in FASTA format

**--host**: FASTA file used to filter our mapping reads

**-n** | **--name**: Output prefix

**--refSites**: Bed file with targeted regions to be used  for coverage checks. Single bases are allowed

**--refVar**: Text file containing default variants loci. Zscore analyses on their DP values will be perfomed.

**--skip_envChecks**: Do not check the presence of conda environments to speed up the starting **[False]**

**--skipPreprocessing**: Skip the GATK4 preprocessing stes **[False]**

**--skip_plots**: To skip plot generation **[False]**

**--skip_ReadSS**: Skip the readSS step and plots **[False]**

**--skip_plotCovigatOff**: Skip the CovigatOff step and plots **[False]**

**--skip_plotUpSet**: Skip the UpSet plot **[False]**

**--skip_snpeff**: Skip snpeff annotation **[False]**

**--keepDuplicates**: Do not remove duplicated reads, just mark them. Be aware that GATK HaplotypeCaller, by default, do not considers duplicated reads, even if they are just marked. In the extreme case in which duplicated reads needs to be used for GATK variant calling, use --skipPreprocessing.

**--StopAtVarCall**: Force the workflow to stop after the variant calling step. That's it. No Variant annotation, No Consensus, No Lineage Assignation, No Report files. Only Vcfs and Filtering plots (from VarStats module) **[False]**

**--periscope**: Perform sgRNA analyses relying on the periscope tool (specific for SARS-CoV-2 data) **[False]**

### Specific of Fasta mode

**--model**: Error model to use with InSilicoSeqISS

**--nreads**: Numer of reads to generate with InSilicoSeq

Please use:

```
$ python PathoSeq-QC.py -h|--help
```

for the full list of options, which include additional CovigatOff parameters.
     
<p align="right">(<a href="#top">back to top</a>)</p>

## Report Examples:

The following plots were made using 1. AY.3 consensus sequence for pure lineage; 2. A 20/80 mix of AY.3/BA.2 reads. 

### Pure lineage

<p align="center">
  <img src="./examples/PurePlots.jpg" width="600">
  <img src="./examples/PureUpset.jpg" width="600">
</p>

### Co-infection (20/80)

<p align="center">
  <img src="./examples/Co2080Plots.jpg" width="600">
  <img src="./examples/Co2080Upset.jpg" width="600">
</p>

<p align="right">(<a href="#top">back to top</a>)</p>

## To Do List

- [ ] Implement the ability to work with long reads data 


<!-- CONTRIBUTING -->
## Contributing

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- LICENSE -->
## License

Distributed under the GNU GENERAL PUBLIC LICENSE. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Gabriele Leoni - [@gabrileoni](https://twitter.com/gabrileoni) - gabriele.LEONI@ec.europa.eu

Project Link: [https://citnet.tech.ec.europa.eu/CITnet/stash/projects/DIGHEALTH/repos/pathoseq-qc/](https://citnet.tech.ec.europa.eu/CITnet/stash/projects/DIGHEALTH/repos/pathoseq-qc/browse)

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

Thanks a lot to the developer of:

* [Covigator](https://github.com/TRON-Bioinformatics/covigator-ngs-pipeline)
* [Periscope](https://github.com/sheffield-bioinformatics-core/periscope)

<p align="right">(<a href="#top">back to top</a>)</p>
