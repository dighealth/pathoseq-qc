#!/usr/bin/pyhon

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This is a python wrapper to lauch the main SARS-Chi.pl script when using condor

import argparse
import os
import subprocess
import glob


parser = argparse.ArgumentParser(description='SARS-Chi2 launcher')
parser.add_argument('-t','--threads', help='Number of threads to use', required=True)
parser.add_argument("--Cluster", help='Cluster ID of Condor Job', required=True)
parser.add_argument("--Process", help='Process ID of Condor Job', required=True)
parser.add_argument('--wd', help="Working directory", required=True)
parser.add_argument('--inputDir',help="Sra input DIR", required=False)
parser.add_argument('--python', help="Python path", required=True)
parser.add_argument('--name', help="Sample name")
parser.add_argument('--condaBin', help="conda bin path", required=True)
parser.add_argument('--condaEnv', help="conda env path", required=True)
parser.add_argument('--fasta', help="Fasta file from which generate in-silico reads", required=False)
parser.add_argument('--scriptDir', help="SARS-Chi absolute path")
parser.add_argument('--memory', help="Amount of memory to use", default = "20g")
parser.add_argument('--nreads', help="Number of reads to generate from fasta consensus file", default=7500000)

args = parser.parse_args()

tmpOutDir = "/scratch2/omics/" + args.Cluster + "_" + args.Process + "/"
os.mkdir(tmpOutDir)
tmpFastqDir = tmpOutDir + "fastq/"
os.mkdir(tmpFastqDir)
OsCommand = "source " + args.condaBin + "/activate " + args.condaEnv + "/default "
OsCommand = OsCommand + "; fasterq-dump -e " + args.threads + " -t " + tmpOutDir + " --split-3 --skip-technical -O " + tmpFastqDir + " " + args.inputDir + "/" + args.name + ".sra"
OsCommand = OsCommand + "; echo " + tmpFastqDir + "; ls -lh " + tmpFastqDir
os.system(OsCommand)

fastq1 = tmpFastqDir + args.name + "_1.fastq"

# 2. Run Crass
if os.path.isfile(fastq1):
   fastq2 = tmpFastqDir + args.name + "_2.fastq"
   Paired = True
else:
   fastq1 = tmpFastqDir + args.name + ".fastq"
   Paired = False

OsCommand = "source " + args.condaBin + "/activate " + args.condaEnv + "/default "
OsCommand = OsCommand + "; " + args.python	+ " " + args.scriptDir + " -t " + args.threads + " --wd " + tmpOutDir + " --memory " + args.memory
if args.name:
	OsCommand = OsCommand + " --name " + args.name
OsCommand = OsCommand + " --fastq1 " + fastq1 + " --fastq2 " + fastq2
OsCommand = OsCommand + " --virstrain --freyja --periscope --debug --condaBin " + args.condaBin + " --condaEnv " + args.condaEnv
OsCommand = OsCommand + "; rsync -rtv " + tmpOutDir + " " + args.wd + " && rm -rf " + tmpOutDir 

os.system(OsCommand)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
   print("PathoSeq-QC pipeline - Gabriele Leoni")
