#!/mnt/jeoproc/envs/OMICS/default/bin/python

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This is a python wrapper to lauch the main SARS-Chi.pl script when using condor

import argparse
import os

parser = argparse.ArgumentParser(description='SARS-Chi2 launcher')
parser.add_argument("--Cluster", help='Cluster ID of Condor Job', required=True)
parser.add_argument("--Process", help='Process ID of Condor Job', required=True)
parser.add_argument('-t','--threads', help='Number of threads to use', required=True)
parser.add_argument('--wd', help="Working directory", required=True)
parser.add_argument('--name', help="Sample name")
parser.add_argument('--fastq1',help="Reads file", required=False)
parser.add_argument('--fastq2',help="Mate file", required=False)
parser.add_argument('--python', help="Python path", required=True)
parser.add_argument('--condaBin', help="conda bin path", required=True)
parser.add_argument('--condaEnv', help="conda env path", required=True)
parser.add_argument('--fasta', help="Fasta file from which generate in-silico reads", required=False)
parser.add_argument('--scriptDir', help="SARS-Chi absolute path")
parser.add_argument('--memory', help="Amount of memory to use", default = "20g")
parser.add_argument('--nreads', help="Number of reads to generate from fasta consensus file", default=7500000)
parser.add_argument('--organism', help="Organism name", required = False)
parser.add_argument('--H5N1', help = "Trigger H5N1 analyses",required = False, action = "store_true")
parser.add_argument('--minDP', help = "Min DP value for DP filtering", required = False)
parser.add_argument('--percDP', help = "Min DP in percentage for DP filtering", required = False)
parser.add_argument('--reference', help = "Abs path to reference genome fasta file", required = False)
parser.add_argument('--host', help = "Abs path to host reference genome fasta file", required = False)
parser.add_argument('--zFilterThreshold', help = "zFilterThreshold", required=False)

args = parser.parse_args()

tmpOutDir = "/scratch2/omics/" + args.Cluster + "_" + args.Process + "/"
os.mkdir(tmpOutDir)

OsCommand = args.condaBin  + "/conda config --add envs_dirs " + args.condaEnv + "/; "
OsCommand = OsCommand + "source " + args.condaBin + "/activate " + args.condaEnv + "/default "
OsCommand = OsCommand + "; " + args.python + " " + args.scriptDir + " -d --skip_ReadSS -t " + args.threads
if args.organism:
    OsCommand = OsCommand + " --organism " + args.organism
if args.reference:
    OsCommand = OsCommand + " --reference " + args.reference
if args.H5N1:
    OsCommand = OsCommand + " --H5N1 "
if args.minDP:
    OsCommand = OsCommand + " --minDP " + args.minDP
if args.percDP:
    OsCommand = OsCommand + " --percDP " + args.percDP
if args.zFilterThreshold:
    OsCommand = OsCommand + " --zFilterThreshold " + args.zFilterThreshold
if args.host:
	OsCommand = OsCommand + " --host " + args.host

OsCommand = OsCommand + " --wd " + tmpOutDir + " --memory " + args.memory 

if args.name:
	OsCommand = OsCommand + " --name " + args.name
if args.fasta:
	OsCommand = OsCommand + " --fasta " + args.fasta + " --nreads " + args.nreads
else:
	OsCommand = OsCommand + " --fastq1 " + args.fastq1 + " --fastq2 " + args.fastq2
OsCommand = OsCommand + " --skip_envChecks --virstrain_2 --debug --condaBin " + args.condaBin + " --condaEnv " + args.condaEnv
OsCommand = OsCommand + "; rsync -rtv " + tmpOutDir + " " + args.wd + " && rm -rf " + tmpOutDir 

print(OsCommand)
os.system(OsCommand)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
   print("PathoSeq-QC pipeline - Gabriele Leoni")

 
