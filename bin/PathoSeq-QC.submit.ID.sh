# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

universe = vanilla

# Set your wd
wd = /your/wd
# Set Pipeline name
PipelineName = bin/PathoSeq-QC.condor.wrapper.ID
# Where to find PathoSeq-QC.py
scriptDir = /your/repo/PathoSeq-QC/PathoSeq-QC.py
# Number of threads to use
threads = 60

LogDir = /your/condor/log/dir/
# where to find python
python = /path/to/python

# FASTA mode parameters
NReads = 5000000
memory = 100g

# Where to find conda Bin and Env paths
condaBin = /path/to/conda/bin 
condaEnv = /path/to/conda/envs

initialdir = $(wd)/results/$(SampleID)
input_dir = /your/sample/repo/ncbi/sra/$(SampleID)
# # FASTA MODE
# arguments = --python $(python) --fasta $(FASTA) --nreads $(NReads) --memory $(memory) --scriptDir $(scriptDir) -t $(threads) --wd $(wd)/results/$(SampleID) --condaBin  $(condaBin) --condaEnv $(condaEnv)
# FASTQ MODE 
arguments = --Cluster $(Cluster) --Process $(Process) --name $(SampleID) --python $(python) --wd $(wd)/results/$(SampleID) --inputDir $(input_dir) --memory $(memory) --scriptDir $(scriptDir) -t $(threads) --condaBin  $(condaBin) --condaEnv $(condaEnv)

executable = $(PipelineName).py
request_memory = $(memory)
when_to_transfer_output = ON_EXIT
priority = 1

# Set the log/out/err dirs
log = $(LogDir)/logs/$(SampleID)_$(Cluster)_$(Process).log
error = $(LogDir)/errs/$(SampleID)_$(Cluster)_$(Process).err
output = $(LogDir)/outs/$(SampleID)_$(Cluster)_$(Process).out

# 
# FASTA MODE
# Queue SampleID, FASTA from Sample.fasta.list

# FASTQ MODE
Queue SampleID from your.ID.list.file

