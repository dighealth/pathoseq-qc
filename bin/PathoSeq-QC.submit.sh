# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

universe = vanilla

# Set your wd
wd = /eos/jeodpp/data/projects/OMICS/leoniga/H5N1
scriptDir = /eos/jeodpp/data/projects/OMICS/leoniga/H5N1/PathoSeq-QC.v1.1/PathoSeq-QC.0.1.py
# Number of threads to use
threads = 30
minDP = 0
percDP = 0
zFilterThreshold = 2
host = "/eos/jeodpp/data/projects/OMICS/leoniga/reference/cattle/cattle.GCF_002263795.3_ARS-UCD2.0_genomic.fna.gz"

LogDir = /mnt/jeoproc/log/omicsproc
# where to find python
python = /mnt/jeoproc/envs/OMICS/default/bin/python

# FASTA mode parameters
memory = 100g

NReads = 500000

# Where to find conda Bin and Env paths
condaBin = /mnt/jeoproc/log/omicsproc/conda/conda/bin
condaEnv = /mnt/jeoproc/envs/OMICS

initialdir = $(wd)/results/$(SampleID)

#FASTQ MODE 
arguments = --Cluster $(Cluster) --Process $(Process) --fastq1 $(FASTQ1) --fastq2 $(FASTQ2) --python $(python) --wd $(wd)/results/$(SampleID) --memory $(memory) --scriptDir $(scriptDir) -t $(threads) --H5N1 --zFilterThreshold $(zFilterThreshold) --minDP $(minDP) --percDP $(percDP) --condaBin  $(condaBin) --condaEnv $(condaEnv)
# FASTA MODE
#arguments = --Cluster $(Cluster) --Process $(Process) --python $(python) --fasta $(FASTA) --nreads $(NReads) --memory $(memory) --scriptDir $(scriptDir) -t $(threads) --wd $(wd)/B3.13.genoflu/$(SampleID) --H5N1 --zFilterThreshold $(zFilterThreshold) --minDP $(minDP) --percDP $(percDP) --condaBin  $(condaBin) --condaEnv $(condaEnv)


executable = /eos/jeodpp/data/projects/OMICS/leoniga/H5N1/PathoSeq-QC.v1.1/bin/PathoSeq-QC.condor.wrapper.py
request_cpus = $(threads)
request_memory = $(memory)
when_to_transfer_output = ON_EXIT
priority = 1

# Set the log/out/err dirs
log = $(LogDir)/logs/$(SampleID)_$(Cluster)_$(Process).log
error = $(LogDir)/errs/$(SampleID)_$(Cluster)_$(Process).err
output = $(LogDir)/outs/$(SampleID)_$(Cluster)_$(Process).out

# Sample.list should be formatted as a tab delimited file, whit no header.
# FASTA MODE
# Queue SampleID, FASTA from B3.13.genoflu.list
# in Fasta mode, 1st column should have the name/prefix of your sample; 2nd column the absolute file path to the input consensus sequence

# FASTQ MODE
Queue SampleID, FASTQ1, FASTQ2 from H5N1.sample.fastq.list2
# In FASTQ mode, first column should be the name/prefix; 2nd and 3rd column should report the absolute file path to the reads and mates fastq files.

