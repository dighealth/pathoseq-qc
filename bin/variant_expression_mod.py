import	vcf
import	pysam
import	argparse
import	pandas	as pd
from	plotnine import *


def check_position_in_bam(bam, position, ext):
	start = int(position) - int(ext)
	end = int(position) + int(ext)
	result = []
	for pileupcolumn in bam.pileup("MN908947.3", start, end	+ 1):
	    if pileupcolumn.pos == start - 1:
	        for pileupread in pileupcolumn.pileups:
	            if not pileupread.is_del and not pileupread.is_refskip:
                        print(pileupcolumn.pos)
                        read_class = pileupread.alignment.get_tag("XC")
                        base = pileupread.alignment.query_sequence[pileupread.query_position]
                        result.append((pileupcolumn.pos, read_class, base))
	return	result


def	main(args):
	vcf_reader = vcf.Reader(open(args.vcf, 'r'))
	bam = pysam.AlignmentFile(args.bam, "rb")

	result = []

	for record in vcf_reader:
		check = check_position_in_bam(bam, record.POS, args.ext)
		result.extend(check)

	exit()

	df = pd.DataFrame(result, columns=['position', 'read_class', 'base'])

	df['position'] = df['position'].astype(str)

	pos_list = df['position'].value_counts().index.tolist()
	pos_cat	= pd.Categorical(df['position'], categories=pos_list)
	df = df.assign(pos_cat=pos_cat)

	x = (ggplot(df, aes(x='pos_cat', y='stat(count/sum(count))', color='base', fill='base')) + \
                geom_bar(position='fill', width=0.8) + \
		facet_wrap(['read_class'], ncol=1) + \
		theme(axis_text_x=element_text(angle=90)))

	x.save(filename=args.output_prefix+"_base_counts.png",	height=5, width=5, units='in', dpi=300)

	df = df.rename(columns={"position": "position", "base": "base"})
	df = df.assign(sample=args.sample)

	df.to_csv(args.output_prefix+"_base_counts.csv",index=False)


if	__name__ == '__main__':
	parser= argparse.ArgumentParser(description='periscope: Get frequencies of bases at variant positions in different read classes')
	parser.add_argument('--periscope-bam',	dest="bam", help='Bam file from periscope')
	parser.add_argument('--output-prefix',	dest='output_prefix', help='Prefix of the output file i.e. <DIRECTORY>/<FILE_PREFIX>')
	parser.add_argument('--vcf',	dest='vcf', help='Non-gz compressed VCF file')
	parser.add_argument('--sample',	help='Sample identifier')
	parser.add_argument('--ext',	help='Expand by -INT and INT nucleotide the VCF POS', default=20)

	args = parser.parse_args()
	main(args)
