#!/usr/bin/pyhon

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from re import sub
import sys
import os
import subprocess
import shutil
from unicodedata import name
import coloredlogs, logging

# CovigatOff python modules


# Check that conda environement exists and that the required software is present
def CondaEnvCheck(condaBin, name):

    logger = logging.getLogger(__name__)

    Command = condaBin + "/conda env list | "
    Command = Command + " grep " + name + " > /dev/null"
    check = os.system(Command)

    if check != 0:
        logger.error('Environment: %s - MISSING!', name)
        sys.exit('Exiting...')
    else:
        logger.info('Environment: %s - Found', name)
    
    return

def do_subprocess(cmd, condaEnv, envName):
    # define env
    env_path = os.path.join(condaEnv, envName)
    subprocess_env = os.environ.copy()
    subprocess_env['PATH'] = os.path.join(env_path, 'bin') + os.pathsep + subprocess_env['PATH']

    #logger = logging.getLogger('Subprocess Env')
    #logger.debug('Subprocess_env: %s', subprocess_env)

    process = subprocess.Popen(cmd,
                               shell=True,
                               env = subprocess_env)
    process.wait()
    return process.returncode

# Trimming with fastp
def Fastp(condaBin, condaEnv, logDir, threads, outDir, fastqDir, name, illumina, **kwargs):
    outPrefix = outDir + "Tabs/" + name
    fastqPrefix = fastqDir + name
    stdOutFile = logDir + name + ".fastp.log"
    stdErrFile = logDir + name + ".fastp.stderr"
    out1 = fastqPrefix + ".trimmed.R1.fq.gz"
    out2 = fastqPrefix + ".trimmed.R2.fq.gz"

    logger = logging.getLogger(__name__)
    logger.info('Fastp step: STARTED')

    if threads >= 16:
        fastpThreads = str(10)
        logger.warning("Working threads reduced from %d to %s for Fastp compatibility", threads, fastpThreads)
    else:
        fastpThreads = str(threads)

    if illumina:
        Command = f"fastp --thread {fastpThreads} --in1 {kwargs['reads']} --in2 {kwargs['mates']} --out1 {out1} --out2 {out2} --json {outPrefix}.fastp.stats.json --html {outPrefix}.fastp.stats.html > {stdOutFile} 2>> {stdErrFile}"
    else:
        Command = f"fastp --thread {fastpThreads} --in1 {kwargs['readsN']} --out1 {out1} --json {outPrefix}.fastp.stats.json --html {outPrefix}.fastp.stats.html > {stdOutFile} 2>> {stdErrFile}"

    logger.debug('Running: %s', Command)
    check=do_subprocess(Command, condaEnv, "fastp")

    if check != 0:
        logger.error('Fastp step: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info('Fastp step: COMPLETED!')
    
    return out1,out2

# Mapping step with BWA and samtools 
def mapping(condaBin, condaEnv, logDir, reads, mates, reference, threads, outDir, name):
    
    outPrefix = outDir + name
    logPrefix = logDir + name
    mappingLog = outPrefix + ".mapping.log"
    stdErrFile = logPrefix + ".bwa.samtools.stderr"
    bam = outPrefix + ".bam"

    logger = logging.getLogger(__name__)
    logger.info('Mapping step: STARTED')

    Command = f"bwa mem -t {threads} {reference} {reads} {mates} 2>> {mappingLog} | samtools view -uS - 2>> {stdErrFile} | samtools sort - > {bam} 2>> {stdErrFile}"
    logger.debug('Running: %s', Command)

    check=do_subprocess(Command, condaEnv, "bwa")
    if check != 0:
        logger.error('Mapping step: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info('Mapping step: COMPLETED!')

    return bam

def mappingN():
    pass

# Performing BAM preprocessing with GATK
def preprocessing(condaBin, condaEnv, logDir, inputBam, reference, outDir, name, memory, spark, skipPreprocessing):
    
    outPrefix = outDir + name
    logPrefix = logDir + name
    stdOutFile = logPrefix + ".preprocessing.log"
    stdErrFile = logPrefix + ".preprocessing.stderr"
    tmpDir = outDir + "tmpDir/"
    bam = outPrefix + ".preprocessed.bam"
    bai = outPrefix + ".preprocessed.bai"
    metrics = outPrefix + ".deduplication.metrics.txt"
    bamDedup = outPrefix + ".deduplicated.bam"

    logger = logging.getLogger(__name__)
    logger.info('Preprocessing step: STARTED')

    if skipPreprocessing == False:
        Command1 = f"gatk CleanSam --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' --INPUT {inputBam} --OUTPUT /dev/stdout 2> {stdOutFile} | gatk AddOrReplaceReadGroups --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' --VALIDATION_STRINGENCY SILENT --INPUT /dev/stdin --OUTPUT {bam} --REFERENCE_SEQUENCE {reference} --RGPU 1 --RGID 1 --RGSM {name} --RGLB 1 --RGPL ILLUMINA --SORT_ORDER queryname > {stdOutFile} 2>> {stdErrFile}"
        if spark:
            if markDuplicates:
                Command2 = f"gatk MarkDuplicatesSpark --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' -I {bam} -M {metrics} -O {bamDedup} --remove-sequencing-duplicates >> {stdOutFile} 2>> {stdErrFile}"
            else:
                Command2 = f"gatk MarkDuplicatesSpark --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' -I {bam} -M {metrics} -O {bamDedup} >> {stdOutFile} 2>> {stdErrFile}"
            Command3 = f"gatk SortSamSpark --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' -I {bamDedup} -O {bam} -SO coordinate >> {stdOutFile} 2>> {stdErrFile}"
        else: 
            Command2 = f"gatk MarkDuplicates --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' --INPUT {bam} --METRICS_FILE {metrics} --OUTPUT {bamDedup} --REMOVE_DUPLICATES {markDuplicates} >> {stdOutFile} 2>> {stdErrFile}"
            Command3 = f"gatk SortSam --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' --INPUT {bamDedup} --OUTPUT {bam} --SORT_ORDER coordinate >> {stdOutFile} 2>> {stdErrFile}"
        Command4 = f"gatk BuildBamIndex --OUTPUT {bai} --INPUT {bam} >> {stdOutFile} 2>> {stdErrFile}"
        Command5 = f"rm -rf {tmpDir}"

        for cmd in [Command1,Command2,Command3,Command4,Command5]:
            logger.debug('Running: %s', cmd)
            check=do_subprocess(cmd, condaEnv, "gatk")
 
            if check != 0:
                logger.error('Preprocessing step: ERROR!')
                sys.exit('Exiting...')
    
        logger.info('Preprocessing step: COMPLETED!')
    else:
        Command1 = f"gatk CleanSam --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' --INPUT {inputBam} --OUTPUT /dev/stdout 2> {stdOutFile} | gatk AddOrReplaceReadGroups --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' --VALIDATION_STRINGENCY SILENT --INPUT /dev/stdin --OUTPUT {bamDedup} --REFERENCE_SEQUENCE {reference} --RGPU 1 --RGID 1 --RGSM {name} --RGLB 1 --RGPL ILLUMINA --SORT_ORDER queryname > {stdOutFile} 2>> {stdErrFile}"
        Command3 = f"gatk SortSam --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' --INPUT {bamDedup} --OUTPUT {bam} --SORT_ORDER coordinate >> {stdOutFile} 2>> {stdErrFile}"
        Command4 = f"gatk BuildBamIndex --OUTPUT {bai} --INPUT {bam} >> {stdOutFile} 2>> {stdErrFile}"
        Command5 = f"rm -rf {tmpDir}"

        for cmd in [Command1, Command3, Command4, Command5]:
            logger.debug('Running: %s', cmd)
            check=do_subprocess(cmd, condaEnv, "gatk")
 
            if check != 0:
                logger.error('AddOrReplaceReadGroups step: ERROR!')
                sys.exit('Exiting...')
    
        logger.info('AddOrReplaceReadGroups step: COMPLETED!')
    
    return bam

# Performing coverage analyses
def coverage(condaBin, condaEnv, logDir, inputBam, outDir, name):

    outPrefix = outDir + name
    logPrefix = logDir + name
    coverageTsv = outPrefix + ".coverage.tsv"
    depthTsv = outPrefix + ".depth.tsv"
    strErrFile = logPrefix + ".coverage.stderr"

    logger = logging.getLogger(__name__)
    logger.info('Coverage analyses: STARTED')

    Command1 = f"samtools coverage {inputBam} > {coverageTsv} 2> {strErrFile}"
    Command2 = f"samtools depth -s -d 0 -H {inputBam} > {depthTsv} 2>> {strErrFile}"
    
    for cmd in [Command1,Command2]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "samtools")
        if check != 0:
            logger.error('Coverage analyses: ERROR!')
            sys.exit('Exiting...')
    
    logger.info('Coverage analyses: COMPLETED!')

    return

# Performing variant calling with LoFreq
def LoFreq(condaBin, condaEnv, logDir, inputBam, threads, reference, outDir, name, minBq, minAltBq, minMq ):
    
    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".lofreq.stderr"
    bam = outPrefix + ".tmp.bam"
    bai = outPrefix + ".tmp.bam.bai"
    vcf = outPrefix + ".lofreq.vcf.gz"

    logger = logging.getLogger(__name__)
    logger.info('LoFreq analyses: STARTED')

    # Lofreq Bug: if vcf exists it ends even with --force-overwrite
    if os.path.exists(vcf):
        os.remove(vcf)

    Command1 = f"lofreq indelqual --dindel --ref {reference} {inputBam} > {bam} 2> {stdErrFile} ; samtools index {bam}; "
    Command2 = f"lofreq call-parallel --pp-threads {threads} --min-bq {minBq} --min-alt-bq {minAltBq} --min-mq {minMq} --ref {reference} --force-overwrite --call-indels --out {vcf} {bam} 2>> {stdErrFile}; "
    Command3 = f"bcftools index {vcf}"
    Command4 = f"rm {bam}; rm {bai}"

    for cmd in [Command1,Command2,Command3,Command4]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "lofreq")
        if check != 0:
            logger.error('LoFreq nalayses: ERROR!')
            sys.exit('Exiting...')
    
    logger.info('LoFreq analyses: COMPLETED!')

    return vcf

# Performing GATK variant calling analyses
def GATK(condaBin, condaEnv, logDir, inputBam, reference, threads, memory, outDir, name, minBq, minMq, spark):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".gatk.stderr"
    tmpDir = outDir + "tmpDir/"
    vcf = outPrefix + ".gatk.vcf"
    vcfGz = outPrefix + ".gatk.vcf.gz"
    BamOut = outPrefix + ".gatk.bamout.bam"

    logger = logging.getLogger(__name__)
    logger.info('GATK analyses: STARTED')


    if os.path.isdir(tmpDir):
        shutil.rmtree(tmpDir)
        os.mkdir(tmpDir)
    else:
        os.mkdir(tmpDir)

    if spark:
        Command1 = (
        f"gatk HaplotypeCallerSpark --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' "
        f"--input {inputBam} --output {vcf} --reference {reference} --native-pair-hmm-threads {threads} "
        f"--ploidy 1 --min-base-quality-score {minBq} --minimum-mapping-quality {minMq} "
        f"--annotation AlleleFraction 2> {stdErrFile} "
        )
    else:
        Command1 = (
        f"gatk HaplotypeCaller --java-options '-Xmx{memory} -Djava.io.tmpdir={tmpDir}' "
        f"-bamout {BamOut} --input {inputBam} --output {vcf} --reference {reference} --native-pair-hmm-threads {threads} "
        f"--ploidy 1 --min-base-quality-score {minBq} --minimum-mapping-quality {minMq} "
        f"--annotation AlleleFraction 2> {stdErrFile} "
        )
    Command2 = f"bcftools view --output-type z {vcf} > {vcfGz}"
    Command3 = f"bcftools index -f {vcfGz}"
    Command4 = f"rm -rf {tmpDir}"


    for cmd in [Command1,Command2,Command3,Command4]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "gatk")
        if check != 0:
            logger.error('GATK analyses: ERROR!')
            sys.exit('Exiting...')
    
    logger.info('GATK analyses: COMPLETED!')

    return vcfGz

# Running VCF2fasta
def vcf2fasta(condaBin, condaEnv, logDir, inputVcf, reference, outDir, name):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".vcf2fasta.stderr"
    fasta = outPrefix + ".fasta"

    logger = logging.getLogger(__name__)
    logger.info("VCF2Fasta: STARTED")


    Command1 = (
    f"bcftools consensus --fasta-ref {reference} --include 'FILTER=\"PASS\" | FILTER=\".\"' "
    f"--output {fasta} {inputVcf} 2> {stdErrFile}; "
    )

    for cmd in [Command1]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "gatk")
        if check != 0:
            logger.error('VCF2Fasta: ERROR!')
            sys.exit('Exiting...')
    
    logger.info("VCF2Fasta: COMPLETED!")

    return fasta

# Running Pangolin
def pangolin(condaBin, condaEnv, logDir, input, threads, outDir, name):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdOutFile = logPrefix + ".pangolin.stdout"
    stdErrFile = logPrefix + ".pangolin.stderr"
    tmpDir = outDir + "tmpDir/"
    pangolinOut = outPrefix + ".pangolin.csv"

    logger = logging.getLogger(__name__)
    logger.info("Pangolin: STARTED")

    Command = (
    f"pangolin {input} --outfile {pangolinOut} --tempdir {tmpDir} "
    f"--threads {threads} > {stdOutFile} 2>> {stdErrFile}"
    f"; rm -rf {tmpDir}")

    logger.debug('Running: %s ', Command)
    
    check=do_subprocess(Command, condaEnv, "pangolin")
    if check != 0:
        logger.error('Pangolin: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info("Pangolin: COMPLETED!")

    return

# Running VarNorm
def varnorm(condaBin, condaEnv, logDir, inputVcf, reference, outDir, name):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".varnorm.stderr"
    vcf = outPrefix + ".normalized.vcf"

    logger = logging.getLogger(__name__)
    logger.info("VarNorm: STARTED")

    Command = (
    f"bcftools sort {inputVcf} 2> {stdErrFile} | "
    f"bcftools norm --multiallelics -any --check-ref e --fasta-ref {reference} --old-rec-tag OLD_CLUMPED - 2>> {stdErrFile} | "
    f"bcftools norm --rm-dup exact -o {vcf} - 2>> {stdErrFile}"
    )

    logger.debug('Running: %s ', Command)

    check=do_subprocess(Command, condaEnv, "gatk")
    if check != 0:
        logger.error('VarNorm: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info("VarNorm: COMPLETED!")

    return vcf

# Annotating SARS-CoV-2 vcf files
def AnnotateSARS(condaBin, condaEnv, logDir, inputVcf, outDir, name, conservation_sarscov2, conservation_sarscov2_header, conservation_sarbecovirus, conservation_sarbecovirus_header,conservation_vertebrate, conservation_vertebrate_header, pfam_names, pfam_names_header, pfam_descriptions, pfam_descriptions_header):

    outPrefix = outDir + name
    logPrefix = logDir + name
    vcf = outPrefix + ".SARS-CoV-2.annotated.vcf"
    stdErrFile = logPrefix + ".SARS-CoV-2.annotations.stderr"
    
    logger = logging.getLogger(__name__)
    logger.info('SARS-CoV-2 annotation: STARTED')

    Command = (
    f"bcftools annotate --annotations {conservation_sarscov2} --header-lines {conservation_sarscov2_header} -c CHROM,FROM,TO,CONS_HMM_SARS_COV_2 --output-type z {inputVcf} 2> {stdErrFile} | "
    f"bcftools annotate --annotations {conservation_sarbecovirus} --header-lines {conservation_sarbecovirus_header} -c CHROM,FROM,TO,CONS_HMM_SARBECOVIRUS --output-type z - 2>> {stdErrFile} | "
    f"bcftools annotate --annotations {conservation_vertebrate} --header-lines {conservation_vertebrate_header} -c CHROM,FROM,TO,CONS_HMM_VERTEBRATE_COV --output-type z - 2>> {stdErrFile} | "
    f"bcftools annotate --annotations {pfam_names} --header-lines {pfam_names_header} -c CHROM,FROM,TO,PFAM_NAME --output-type z - 2>> {stdErrFile} | "
    f"bcftools annotate --annotations {pfam_descriptions} --header-lines {pfam_descriptions_header} -c CHROM,FROM,TO,PFAM_DESCRIPTION - > {vcf} 2>> {stdErrFile}; "
    )

    logger.debug('Running: %s', Command)

    check=do_subprocess(Command, condaEnv, "gatk")
    if check != 0:
        logger.error('SARS-CoV-2 annotation: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info('SARS-CoV-2 annotation: COMPLETED!')

    return vcf

# Running Vafator
def vafator(condaBin, condaEnv, logDir, inputVcf, bam, mq, bq, outDir, name):

    outPrefix = outDir + name
    logPrefix = logDir + name
    vcf = outPrefix + ".vaf.vcf"
    stdErrFile = logPrefix + ".vafator.stderr"

    logger = logging.getLogger(__name__)
    logger.info('Vafator step: STARTED')


    Command = (
    f"vafator --input-vcf {inputVcf} --output-vcf {vcf} --bam vafator {bam} "
    f"--mapping-quality {mq} --base-call-quality {bq} 2> {stdErrFile};"
    )

    logger.debug('Running: %s', Command)

    check=do_subprocess(Command, condaEnv, "vafator")
    if check != 0:
        logger.error('Vafator step: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info('Vafator step: COMPLETED!')

    return vcf

# Annotating Vafator Vcf
def annotateVafator(condaBin, condaEnv, logDir, inputVcf, outDir, name, low_freq_threshold, subclonal_threshold):

    outPrefix = outDir + name
    logPrefix = logDir + name
    vcf = outPrefix + ".vaf.annotated.vcf"
    stdErrFile = logPrefix + ".annotation.stderr"

    logger = logging.getLogger(__name__)
    logger.info('Vafator Annotating step: STARTED')

    Command = (
    f"bcftools view -Ob {inputVcf} | "
    f"bcftools filter --exclude 'INFO/vafator_af < {low_freq_threshold}' --soft-filter LOW_FREQUENCY - 2> {stdErrFile} | "
    f"bcftools filter --exclude 'INFO/vafator_af >= {low_freq_threshold} && INFO/vafator_af < {subclonal_threshold}' --soft-filter SUBCLONAL --output-type v - > {vcf} 2>> {stdErrFile};"
    )

    logger.debug('Running: %s', Command)
    check=do_subprocess(Command, condaEnv, "gatk")
    if check != 0:
        logger.error('Vafator Annotating step: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info('Vafator Annotating step: COMPLETED!')

    return vcf

# Phasing step
def phasing(script, fasta, gtf, inputVcf, outDir, name):

    outPrefix = outDir + name
    stdErrFile = outPrefix + ".phasing.stderr"
    vcf = outPrefix + ".phased.vcf"

    logger = logging.getLogger(__name__)
    logger.info('Phasing step: STARTED')

    Command = script + " --fasta " + fasta + " --gtf " + gtf + " --input-vcf " + inputVcf + " --output-vcf " + vcf + " 2> " + stdErrFile + ";"

    logger.debug('Running: %s', Command)

    check = os.system(Command)

    if check != 0:
        logger.error('Phasing step: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info('Phasing step: COMPLETED!')

    return vcf

# Annotate vcf
def snpeff(condaBin, condaEnv, logDir, inputVcf, outDir, name, snpeffConfig, snpeffData, organism):

    outPrefix = outDir + name
    logPrefix = logDir + name
    vcf = outPrefix + ".snpeff.annotated.vcf"
    stdErrFile = logPrefix + ".snpeff.annotate.stderr"

    logger = logging.getLogger(__name__)
    logger.info('Annotating step: STARTED')

    Command1 = f"cp {snpeffConfig} ./; "
    Command2 = (
        f"snpEff eff -dataDir {snpeffData} -noStats -no-downstream -no-upstream "
        f"-no-intergenic -no-intron -onlyProtein -hgvs1LetterAa -noShiftHgvs "
        f"{organism} {inputVcf} > {vcf} 2> {stdErrFile}; "
        f"rm snpEff.config"
    )

    for cmd in [Command1,Command2]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "snpeff")

        if check != 0:
            logger.error('Annotating step: ERROR!')
            sys.exit('Exiting...')
    
    logger.info('Annotating step: COMPLETED!')

    return vcf

# Compress
def compress(condaBin, condaEnv, inputVcf):

    vcf = inputVcf + ".gz"

    logger = logging.getLogger(__name__)
    logger.info('Compression step: STARTED')

    Command1 = f"bgzip -f {inputVcf}"
    Command2 = f"tabix -f -p vcf {vcf}"

    for cmd in [Command1,Command2]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "gatk")

        if check != 0:
            logger.error('Compression step: ERROR!')
            sys.exit('Exiting...')
    
    logger.info('Compression step: COMPLETED!')

    return vcf

# Variant calling from fasta consensus
def assVarCall(script, fasta, reference, matchScore, mismatchScore, openGapScore, extendedGapScore, outDir, name):

    outPrefix = outDir + name
    stdErrFile = outPrefix + ".assembly.variant.caller.stderr"
    vcf = outPrefix + ".vcf"

    logger = logging.getLogger(__name__)
    logger.info('Variant Calling from FASTA step: STARTED')

    Command = script + " --fasta " + fasta + " --reference " + reference + " --output-vcf " + vcf + " --match-score " + matchScore + " --mismatch-score " + mismatchScore + " --open-gap-score " + openGapScore + " --extend-gap-score " + extendedGapScore + " 2> " + stdErrFile + ";"

    logger.debug('Running: %s', Command)

    check = os.system(Command)

    if check != 0:
        logger.error('Variant Calling from FASTA step: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info('Variant Calling from FASTA step: COMPLETED!')

    return vcf
