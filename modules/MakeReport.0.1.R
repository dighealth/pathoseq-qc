# This script will generate a report for the SARS-Chi pipeline

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Libraries
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(ggrepel))
suppressPackageStartupMessages(library(patchwork))
suppressPackageStartupMessages(library(argparse))
suppressPackageStartupMessages(library(UpSetR))
suppressPackageStartupMessages(library(RColorBrewer))

parser <- ArgumentParser()

# specify our desired options 
# by default ArgumentParser will add an help option 
parser$add_argument("--wd", help="Working Directory", default = "./")
parser$add_argument("-n", "--name", help="Sample ID", default = "SampleName")
parser$add_argument("--varDB", help="Variants Database path")

parser$add_argument("--refVar", help="RefVar file path")
parser$add_argument("--refSites", help="RefSites files path")

parser$add_argument("--zFilterThreshold", help = "zScore threshold based on DP distribution at Ref Sites")
parser$add_argument("--minAF", help="Minimal AF to keep variants during filtering steps")

parser$add_argument("--skip_plotsUpSet", help="Skip UpSet Diagram generation", action = "store_true", default = FALSE)
parser$add_argument("--skip_readSS", help="Skip ReadSS Plot", action = "store_true",default = FALSE)
parser$add_argument("--skip_plotsAF", help="Skip AF Plots", action = "store_true",default = FALSE)
parser$add_argument("--skip_plots", help="Do not generate plots", action = "store_true",default = FALSE)

# parser$add_argument("--DP", action = "store_true", default = FALSE)

# get command line options, if help option encountered print help and exit,
# otherwise if options not found on command line then set defaults, 
args <- parser$parse_args()

# Variables
wd = args$wd
name = args$name
varDBpath = args$varDB

zFilterThreshold = as.numeric(args$zFilterThreshold)
minAF = as.numeric(args$minAF)

refVarFile = args$refVar
refSiteFile = args$refSites

skip_plotsAF = args$skip_plotsAF
skip_readSS = args$skip_readSS
skip_plotsUpSet = args$skip_plotsUpSet
skip_plots = args$skip_plots

skip_plotsReadSS = FALSE
if(skip_plots){
  skip_plotsReadSS = TRUE
  skip_plotsAF = TRUE
  skip_plotsUpSet = TRUE
}
if(skip_readSS){
  skip_plotsReadSS = TRUE
}

# Dir paths
ReadSSDir = paste0(wd, "/ReadStrainScanner")
CovigatOffDir = paste0(wd, "/CovigatOff")
VirStrainDir = paste0(wd, "/Virstrain")
FreyjaDir = paste0(wd, "/Freyja")
PangoDir = paste0(CovigatOffDir, "/Pangolin")
LABELDir = paste0(wd, "/LABEL")
GenoFluDir = paste0(wd,"/GenoFlu")

ReportDir = paste0(wd, "/Report")

# Inputs paths
VarDataPath = paste0(ReportDir,"/",name,".ReportVarData.tab")
VarReport = paste0(ReportDir,"/",name,".ReportVarCov.txt")
SitesReport = paste0(ReportDir,"/",name,".ReportSiteCov.txt")

CovOut = paste0(CovigatOffDir,"/",name,".depth.tsv")
PangolinOut = paste0(PangoDir,"/",name,".gatk.pangolin.csv")
FreyjaOut= paste0(FreyjaDir,"/Freyja_report.txt")
LABELOut = paste0(LABELDir,"/",name,".gatk.LABEL_final.txt")
GenoFluOut = paste0(GenoFluDir,"/",name,".gatk.GenoFlu.out")
VirstrainOut1 = paste0(VirStrainDir,"/",name,".gatk.VirStrainReport.txt")
VirstrainOut2 = paste0(VirStrainDir,"/",name,".gatk.VirStrainContigReport.txt")

Stats = paste0(ReadSSDir,"/Report/",name,".scafstats")

# Output paths
UpsetReport = paste0(ReportDir,"/",name,".PlotsUpset.pdf")
PlotsReport = paste0(ReportDir,"/",name,".Plots.pdf")

MainReport = paste0(ReportDir,"/",name,".Report.tab")
OverlapReport = paste0(ReportDir,"/",name,".ReportOverlaps.txt")
ReportVarDB = paste0(ReportDir,"/",name,".ReportVarDB.tab")

custom_theme = theme_light() + theme(plot.title = element_text(size = 18, face = "bold", margin=margin(20,0,20,0), hjust = 0.5)
                                     , axis.title = element_text(size = 16), axis.text = element_text(size = 15)
                                     , axis.title.x = element_text(margin = margin(20,0,0,0))
                                     , axis.title.y = element_text(margin=margin(0,20,0,0))
                                     , plot.margin = unit(c(2,2,2,2),"cm")
                                     , legend.position = "null")

# Load Variant Database
if(file.exists(varDBpath)){
    VarDB = fread(file = varDBpath, sep = "\t", quote = "", header = F, col.names = c("var","lineage"))
    VarDB[, Chrom := sub("_.+","" ,var)]
}
# Load Variants Data es: SRR28752635.ReportVarData.tab
VarData = fread(file = VarDataPath, sep = "\t", quote = "", header = T)
if(nrow(VarData) == 0){
  NoVarData = TRUE
  GatkPassCount = 0
  GatkPassPerc = 0
  GatkDPPassCount = 0
  GatkAFPassCount = 0
  GatkZPassCount = 0
  LoFreqDPPassCount = 0
  LoFreqAFPassCount = 0
  LoFreqZPassCount = 0
} else {
  NoVarData = FALSE
  
  GatkCount = nrow(VarData[ Caller == "GATK",])
  GatkPassCount = nrow(VarData[ Caller == "GATK" & Filter == "PASS",])
  GatkPassPerc = GatkPassCount * 100 / GatkCount
  
  # Evaluate counts for DP.PASS, AF.PASS and the others, to print as output
  GatkDPPassCount = nrow(VarData[ Caller == "GATK" & DP.PASS == "PASS",])
  GatkAFPassCount = nrow(VarData[ Caller == "GATK" & AF.PASS == "PASS",])
  GatkZPassCount = nrow(VarData[ Caller == "GATK" & DP.zScore.PASS == "PASS",])
  
  LoFreqCount = nrow(VarData[ Caller == "LoFreq",])
  LoFreqPassCount = nrow(VarData[ Caller == "LoFreq" & Filter == "PASS",])
  LoFreqPassPerc = LoFreqPassCount * 100 / LoFreqCount
  LoFreqDPPassCount = nrow(VarData[ Caller == "LoFreq" & DP.PASS == "PASS",])
  LoFreqAFPassCount = nrow(VarData[ Caller == "LoFreq" & AF.PASS == "PASS",])
  LoFreqZPassCount = nrow(VarData[ Caller == "LoFreq" & DP.zScore.PASS == "PASS",])
  
}

# Load Coverage Data - mean - sd - median 
Cov.data = fread(file = CovOut, sep = "\t", quote ="", header = T, col.names = c("CHROM","POS","N"))
Cov.mean = as.integer(Cov.data[, mean(as.integer(N))])
Cov.median = as.integer(Cov.data[, median(as.integer(N))])
Cov.sd = as.numeric(Cov.data[, sd(as.numeric(N))])

# Load RefVar and RefSites and check if all PASS - then print N of NoPass and Percentage NoPass
RefSitesCheck = "NA"
if(file.exists(SitesReport)){
  refSitesData = fread(file = SitesReport, sep = "\t", quote = "", header = T)
  RefSitesCheck = "PASS"
  if(sum((refSitesData$zScore < -zFilterThreshold) | (refSitesData$zScore > zFilterThreshold))){
        RefSitesCheck = "NOPASS"
  }
}
RefVarsCheck = "NA"
if(file.exists(VarReport)){
  refVarsData = fread(file = VarReport, sep = "\t", quote = "", header = T)
  RefVarsCheck = "PASS"
  if(sum((refVarsData$zScore < -zFilterThreshold) | (refVarsData$zScore > zFilterThreshold))){
      RefVarsCheck = "NOPASS"
  }
}

# Loading readSS output data
if(! (skip_readSS)){
  Stats.data = fread(file = Stats, sep = "\t", quote = "", header = T)
  names(Stats.data) = c("name","unambiguousReadsFreq","unambiguousMB","ambiguousReadsFreq","ambiguousMB","unambiguousReads","ambiguousReads","assignedReads","assignedBases"  )
  Stats.data[, Lineage := gsub(x = name, pattern = " .+", replacement = "")]
  Stats.data[, ID := Lineage]
  if(nrow(Stats.data) > 5){
    Stats.data.top = Stats.data[1:5,]
  } else { 
    Stats.data.top = Stats.data
  }
  if(! (skip_plotsReadSS)){
    p = ggplot(Stats.data.top, aes(
      x = Lineage
    ))
    p = p + geom_bar(aes( y = unambiguousReadsFreq, fill = Lineage), stat = "identity")
    p = p + geom_point(aes(y = ambiguousReadsFreq))
    p = p + scale_y_continuous(
      # Features of the first axis
      name = "UnAmbiguous Reads Frequency (%)",
      # Add a second axis and specify its features
      sec.axis = sec_axis(~., name="Ambiguous Reads Frequency (%)")
    ) 
    
    p = p + custom_theme
    p = p + theme(
      axis.text.x = element_text(angle = 90), 
      legend.position = "bottom"
    )
    p = p + labs(
      title = paste0("Top 5 UnAmbiguousReads Lingeages + Pangolin Lineage\n",name)
      , x = "", fill = "Lineages"
    )
    p = p + scale_fill_brewer(palette = "Set1")
    p = p + scale_color_brewer(palette = "Set1")
    
    ReadSS_plot = p
  }
} else {
  Stats.data.top = c()
}

# Print the AF values across the genome (GATK and LoFreq)
if (!((skip_plotsAF | skip_plots) & NoVarData)){
  a = ggplot(VarData, aes(
    x = POS
    , y = AF * 100
    , color = Caller
    , shape = OverlappingCall
  ))
  a = a + geom_point(alpha = 0.7)
  a = a + custom_theme
  a = a + labs(
    title = paste0("Alternative alleles Frequency (AF) - ",name),
    x = "Genomic Position",
    y = "AF (%)",
    color = "", OverlappingCall = "Variant Called by Both"
  )
  a = a + geom_hline(yintercept = minAF*100, color = "Green", linetype = "dashed")
  a = a + geom_hline(yintercept = 20, color = "Red", linetype = "dashed")
  a = a + theme(
    legend.position = "bottom"
    , strip.background = element_rect( fill = "black")
    , strip.text.x = element_text(colour = "white")
  )
  a = a + scale_color_brewer(palette = "Set1")
  a = a + facet_wrap(~CHROM, scales = "free_x")
  AF_plot = a

}

# pass the list of tools used to assign lineages
# or check the presence of the output files which might be better

# SARS-CoV-2
# Pangolin
# Load the pangolin data if present
if(file.exists(PangolinOut)){
  Pangolin.data = fread(file = PangolinOut, sep = ",", quote = "", header =T)
  # Extract The frequency of variants of the Pangolin result
  PangolinLineage = Pangolin.data$lineage
  PangolinScore = Pangolin.data$conflict
  
  # Check Pangolin result. Extract Pangolin lineage data if present 
  if( (! (PangolinLineage == "Unassigned" | NoVarData)) & file.exists(varDBpath)) {
    PangolinVar = VarDB[lineage == PangolinLineage, ]
    PangolinVarData = VarData[ Var %in% PangolinVar$var, ]
    
    if(nrow(PangolinVarData) == 0){
      NoPangolinVarData = 1
      print(paste0("WARNING! No VarDB data for lineage: ", PangolinLineage," - This could be related to the reference consensus file" ))
    } else {
      NoPangolinVarData = 0
    }
    NoPangolinData = 0 
    if(! (skip_readSS)){
      PangolinStats = Stats.data[ Lineage %in% PangolinLineage, ]
      PangolinStats[, Lineage := paste0(PangolinLineage,"-Pango")]
      PangolinStats[, ID := PangolinLineage]
      Stats.data.top = rbind(Stats.data.top,PangolinStats)
      
      if(nrow(PangolinStats) == 0){
        # It means that the pangolin resulting lineage is not within the VarDB
        print(paste0("WARNING! No bbmap data for lineage: ",PangolinLineage," - This could be due to the absence of UnAmbiguous reads for that particular lineage!"))
        NoPangolinData = 1
      }
    }
  } else {
    NoPangolinData = 1
    NoPangolinVarData = 1
  }
} else {
  Pangolin.data = data.table()
  PangolinLineage = NA
  PangolinScore = NA
  NoPangolinData = 1
  NoPangolinVarData = 1
}
# Freyja
if(file.exists(FreyjaOut)){
  cmd = paste0("cut -f 2 ", FreyjaOut , " | cut -f 1 -d ' ' | tail -n +3 | head -n 2")
  FreyjaLinData = system(cmd, intern = TRUE)
  FreyjaLin = FreyjaLinData[1]
  FreyjaLinFreq = FreyjaLinData[2]
  cmd = paste0("cut -f 2 ", FreyjaOut, " | cut -f 1 -d ')' | head -n 2 | tail -n +2 | sed -r -e 's/\\[\\(//' -e 's/, /\t/g' -e s/\"'\"//g")
  FreyjaVOCData = system(cmd, intern = TRUE)
  vector = strsplit(FreyjaVOCData, "\t")
  FreyjaVOC = vector[[1]][1]
  FreyjaVOCFreq = vector[[1]][2]
} else {
  FreyjaLin = NA
  FreyjaLinFreq = NA
  FreyjaVOC = NA
  FreyjaVOCFreq = NA
}

# H5N1
# LABEL
if(file.exists(LABELOut)){
  LABELData = fread(file = LABELOut, sep = "\t", quote = "", header = T)
  LABELLin = LABELData[ CLADE != "UNRECOGNIZABLE", CLADE ]
  LABELLin = LABELLin[! duplicated(LABELLin)]
} else {
  LABELLin = NA
} 
# GenoFlu
if(file.exists(GenoFluOut)){
  cmd = paste0("cat ", GenoFluOut," | tail -n 2 | head -n 1 | cut -f 1 -d ':' | cut -f 4 -d ' '")
  GenoFluLin = system(cmd, intern = TRUE)
} else {
  GenoFluLin = NA
} 
# All
# Virstrain
if(file.exists(VirstrainOut1)){
  cmd = paste0("grep -m 1 -A 1 '>>' ", VirstrainOut1, " | cut -f 3,6,7,8 | sed -r 's/>//' | tail -n +2")
  VirstrainData = system( cmd, intern = TRUE)
  vector = strsplit(VirstrainData, "\t")
  VirstrainBest = vector[[1]][1]
  VirstrainMapScore = vector[[1]][2]
  VirstrainValidMap = vector[[1]][3]
  VirstrainTotalMap = vector[[1]][4]
} else {
  VirstrainBest = NA
  VirstrainMapScore = NA
  VirstrainValidMap = NA
  VirstrainTotalMap = NA
}
if(file.exists(VirstrainOut2)){
  cmd = paste0("cut -f 2 ", VirstrainOut2, " | grep -v 'NA' | tail -n +2 | cut -f 1 -d ':' | sort | uniq -c | sed -r -e 's/ +/\t/g' -e 's/\t+//' | tail -n 1 | cut -f 2")
  VirstrainContigBestSp = system( cmd, intern = T)
  cmd = paste0("cut -f 3 ", VirstrainOut2, " | grep -v 'Skip' | tail -n +2 | cut -f 1 -d '|' | sort | uniq -c | sed -r -e 's/ +/\t/g' -e 's/\t+//' | tail -n 1 | cut -f 2")
  VirstrainContigBestSt = system( cmd, intern = T)
} else {
  VirstrainContigBestSp = NA
  VirstrainContigBestSt = NA
}

# Custom Lineage Approach
if(GatkPassCount != 0 & file.exists(varDBpath)){
  
  # Create the Overlaps data
  overlapCounts = data.table(lineages = levels(as.factor(VarDB$lineage)), SampleInLineageOutTotSample = 0, SampleInLineageOutTotLineage = 0)
  
  overlapCountsChr = data.table(lineages = rep(levels(as.factor(VarDB$lineage)), length(levels(as.factor(VarDB$Chrom)))),
                              chrom = rep(levels(as.factor(VarDB$Chrom)), length(levels(as.factor(VarDB$lineage)))),
                              SampleInLineageOutTotSample = 0, SampleInLineageOutTotLineage = 0)
  
  sampleVar = VarData[Caller == "GATK" & Filter == "PASS", as.character(Var)]
  TotSampleVar = length(sampleVar)
  
  for(lin in overlapCounts$lineages){
    TotLinVar = VarDB[ lineage == lin, .N]

    overlapCounts[ lineages == lin, SampleInLineageOutTotSample := sum(sampleVar %in% VarDB[lineage == lin, var]) * 100 / TotSampleVar]
    overlapCounts[ lineages == lin, SampleInLineageOutTotLineage := sum(sampleVar %in% VarDB[lineage == lin, var]) * 100 / TotLinVar]
  
    LinVarByChr = VarDB[ lineage == lin, .N , by = "Chrom"]
    overlapCountsChr[ lineages == lin, SampleInLineageOutTotSample := 0]
    overlapCountsChr[ lineages == lin, SampleInLineageOutTotLineage := 0]
    for(chr in levels(as.factor(overlapCountsChr$chrom))){
      NvarInOverlap = sum(sampleVar %in% VarDB[lineage == lin & Chrom == chr, var])
      if(NvarInOverlap != 0){
        NvarSample = VarData[ Caller == "GATK" & Filter == "PASS" & CHROM == chr, .N]
        NvarLin = LinVarByChr[Chrom == chr,N]
        if(NvarSample != 0){
          overlapCountsChr[ lineages == lin & chrom == chr, SampleInLineageOutTotSample := NvarInOverlap * 100 / NvarSample]
        }
        if(NvarLin){
          overlapCountsChr[ lineages == lin & chrom == chr, SampleInLineageOutTotLineage := NvarInOverlap * 100 / NvarLin]
        }
      }
    }
    
  }
  
  A_B = overlapCounts[ order(-SampleInLineageOutTotSample), c(1,2)]
  B_A = overlapCounts[ order(-SampleInLineageOutTotLineage), c(1,3)]
  
  values = levels(as.factor(A_B$SampleInLineageOutTotSample))
  i = length(values)
  for(val in values){
    A_B[SampleInLineageOutTotSample == val, score_AB := i]
    i = i - 1
  }
  
  keycol = c("SampleInLineageOutTotSample","chrom")
  A_B.chr = setorderv(overlapCountsChr[, c(1,2,3)],keycol,order = -1) 
  for(ch in levels(as.factor(A_B.chr$chrom))){
    values = levels(as.factor(A_B.chr$SampleInLineageOutTotSample))
    i = length(values)
    for(val in values){
      A_B.chr[SampleInLineageOutTotSample == val, score_AB := i]
      i = i - 1
    }
  }
  
  values = levels(as.factor(B_A$SampleInLineageOutTotLineage))
  i = length(values)
  for(val in values){
    B_A[SampleInLineageOutTotLineage == val, score_BA := i]
    i = i - 1
  }
  
  keycol = c("SampleInLineageOutTotLineage","chrom")
  B_A.chr = setorderv(overlapCountsChr[, c(1,2,4)],keycol,order = -1) 
  for(ch in levels(as.factor(B_A.chr$chrom))){
    values = levels(as.factor(B_A.chr$SampleInLineageOutTotLineage))
    i = length(values)
    for(val in values){
      B_A.chr[SampleInLineageOutTotLineage == val, score_BA := i]
      i = i - 1
    }
  }
  
  overlapCounts = merge(A_B, B_A, by = "lineages")
  overlapCounts[, score := score_AB + score_BA]
  TopOverlapCounts = overlapCounts[ order(score),]
  TopOverlapCounts$chrom = "Whole Genome"
  
  overlapCountsChr = merge(A_B.chr, B_A.chr, by = c("lineages","chrom"))
  overlapCountsChr[, score := score_AB + score_BA]
  TopOverlapCountsChr = overlapCountsChr[ order(score),]
  
  TopOverlapCounts = rbind(TopOverlapCounts,TopOverlapCountsChr)
  
  fwrite(TopOverlapCounts, OverlapReport, sep = "\t",quote = F, col.names = T)

  CustomTopLineage = TopOverlapCounts[1, lineages]
  SampleInLineageOutTotSample = TopOverlapCounts[1, SampleInLineageOutTotSample]
  SampleInLineageOutTotLineage = TopOverlapCounts[1, SampleInLineageOutTotLineage]
  CustomTopLineageScore = TopOverlapCounts[1, score]
  
  # Create the plot
  if (!(skip_plots)){
    c = ggplot(TopOverlapCounts, aes(
      x = SampleInLineageOutTotSample
      , y = SampleInLineageOutTotLineage
      , col = score
    ))
    c = c + geom_point(alpha = 0.4)
    c = c + custom_theme
    c = c + labs(
      title = paste0("Custom Lineage assignation approach frequencies - ",name),
      x = "Percentage of sample's mutations per Lineage ",
      y = "Percentage of lineage's mutations per Lineage",
      col = "Score Index"
    )
    c = c + theme(
      legend.position = "bottom"
      , strip.background = element_rect(color = "black", fill = "black"),
      strip.text = element_text(color = "white", face = "bold"))
    c = c + geom_text_repel(data = TopOverlapCounts[, .SD[1:5,] , by = chrom], aes(label = lineages), max.overlaps = 5)
    c = c + scale_x_continuous(limits = c(0,100))
    c = c + scale_y_continuous(limits = c(0,100))
    c = c + facet_wrap(~chrom)
    Overl_plot = c 
  }
} else {
  CustomTopLineage = NA
  SampleInLineageOutTotSample = NA
  SampleInLineageOutTotLineage = NA
  CustomTopLineageScore = NA
}

# Searching for private lineage variants and unique variants within the sample
# Generate the output table with lineage variants counts
LineagePrivateVariantsObserved = NA
UnknownVariantsCount = NA
if(GatkPassCount != 0 & file.exists(varDBpath)){
  VarDBOut = VarDB
  VarDBOut[, CountsOfLineagesWithVar := .N, by = c("var")]
  VarDBOut[, VarIsLineageSpecific := "No"]
  # Add if Variants are specific of a single lineage
  VarDBOut[CountsOfLineagesWithVar == 1, VarIsLineageSpecific := "Yes"]
  VarDBOut[, ObservedInSample := "No"]
  # Add if Variants were observed in the sample
  VarDBOut = VarDB[ var %in% VarData[ Caller == "GATK" & Filter == "PASS",Var], ObservedInSample := "Yes"]
  # Add to the output all variants not yet observed in lineages
  for(NewVar in as.character(VarData[ ! (Var %in% VarDBOut$var) & Caller == "GATK" & Filter == "PASS", Var])){
    NewEntry = data.table(Chrom = sub("_.+","" ,NewVar), var = NewVar, lineage = "Unknown", CountsOfLineagesWithVar = 0, ObservedInSample = "Yes", VarIsLineageSpecific = "Unknown")
    VarDBOut = rbind(VarDBOut, NewEntry)
  }
  VarDBOut[, Pos := gsub(x = var, replacement = "", pattern = ":(.+)" )]
  
  if(length(VarDBOut[ lineage == "Unknown", var] != 0)){
    UnknownVariantsCount = VarDBOut[ lineage == "Unknown", .N]
  }
  if(nrow(VarDBOut[ ObservedInSample == "Yes" & CountsOfLineagesWithVar == 1]) != 0){
    LineagePrivateVariantsObserved = VarDBOut[ ObservedInSample == "Yes" & CountsOfLineagesWithVar == 1, .N]
  }
  # writing the output
  fwrite(VarDBOut, ReportVarDB, sep = "\t", quote = F, col.names = T )
}


#######
# Creating the C plot data
if((! (skip_readSS | GatkPassCount == 0)) & file.exists(varDBpath)){
  Counts.data = data.table(Lineage = Stats.data.top$ID, ID = Stats.data.top$Lineage)
  
  if(! (is.na(PangolinLineage))){
    add = data.table(Lineage = PangolinLineage, ID = paste0(PangolinLineage,"-Pango"))
    Counts.data = rbind(Counts.data, add)
  }
  
  for(lin in Counts.data$Lineage){
    Counts.data[ Lineage == lin, Total.Lineage.Var := VarDB[ lineage == lin, .N]]
    sub = VarData[ as.character(Var) %in% VarDB[ lineage == lin, var] & Caller == "GATK", ]
    Counts.data[ Lineage == lin, Found.Lineage.Var := sub[,.N]]
  }
  
  Counts.data[, Type := "Top"]
  
  if(! (is.na(PangolinLineage))){ 
    Counts.data[nrow(Counts.data), Type := "Pangolin"]
  }
  
  VarList = as.character(VarData[ Caller == "GATK", Var])
  Counts.data[, Total.Sample.GATK.Var := length(VarList)]
  
  Counts.data[, Freq.Lineage := Found.Lineage.Var * 100 / Total.Lineage.Var]
  Counts.data[, Freq.Sample := Found.Lineage.Var * 100 / Total.Sample.GATK.Var]
  
  Counts.data = as.data.table(melt(Counts.data,id.vars = c("Lineage","ID","Type","Total.Lineage.Var","Found.Lineage.Var","Total.Sample.GATK.Var"), measure.vars = c("Freq.Lineage","Freq.Sample")))
  Counts.data[ variable == "Freq.Lineage", variable := "Lineage"]
  Counts.data[ variable == "Freq.Sample", variable := "Sample"]
  
  Counts.data.Lineage = Counts.data[variable == "Lineage",]
  Counts.data.sample = Counts.data[variable == "Sample",]
  
  Counts.data = Counts.data[,c(1:6)]
  Counts.data = Counts.data[! duplicated(Counts.data),]
  
  Counts.data = as.data.table(melt(Counts.data, id.vars = c("Lineage","ID","Type"), measure.vars = c("Total.Lineage.Var","Found.Lineage.Var","Total.Sample.GATK.Var")))
  Raw.Counts.data.Lineage = Counts.data[variable == "Total.Lineage.Var" | variable == "Found.Lineage.Var",]
  Raw.Counts.data.Sample = Counts.data[variable == "Total.Sample.GATK.Var" | variable == "Found.Lineage.Var",]
  
  Raw.Counts.data.Lineage[ variable == "Total.Lineage.Var", variable := "Total Lineage"]
  Raw.Counts.data.Lineage[ variable == "Found.Lineage.Var", variable := "Observed"]
  Raw.Counts.data.Sample[ variable == "Found.Lineage.Var", variable := "Observed"]
  Raw.Counts.data.Sample[ variable == "Total.Sample.GATK.Var", variable := "Total Called"]
  
  Freq.data = rbind(Counts.data.Lineage, Counts.data.sample)
  Freq.data = Freq.data[, c(1,2,3,7,8)]
  
  Raw.Counts.data = rbind(Raw.Counts.data.Lineage, Raw.Counts.data.Sample)
  Raw.Counts.data = Raw.Counts.data[! duplicated(Raw.Counts.data),]


# UpsetR
  if(! (skip_plots)){
  
    if(! (skip_plotsUpSet)){
      subs=c()
      for(lin in Counts.data$Lineage){
        sub = VarDB[lineage == lin, var]
        subs = c(subs,sub)
      }
      # Add Sample Var to Stats.data.top var list (Limit to GATK variants)
      subs = c(subs,levels(as.factor(VarData[Caller == "GATK", as.character(Var)])))
      rows = subs[! duplicated(subs)]
    
      UpSetData = data.table(Identifiers = rows)
    
      # Populating UpSetData top lineages
      for(col in Counts.data$Lineage){
        UpSetData[, eval(col) := 0]
        sub = VarDB[lineage == col, var]
        UpSetData[ Identifiers %in% sub, eval(col) := 1]
      }
      # Populating UpSetData with sample data
      if(! is.na(PangolinLineage)){
        if(name %in% colnames(UpSetData) | name == PangolinLineage){
          print(paste0("WARNING! Sample Name identical to SARS-CoV-2 lineage names or Pangolin output"))
          upset.name = paste0("Sample: ",name)
          UpSetData[, eval(upset.name) := 0]
          UpSetData[Identifiers %in% levels(as.factor(as.character(VarData[Caller == "GATK", Var]))), eval(upset.name) := 1]
        } else {
          UpSetData[, eval(name) := 0]
          upset.name = name
          UpSetData[ Identifiers %in% levels(as.factor(as.character(VarData[ Caller == "GATK", Var]))), eval(name) := 1]
        }
      } else {
        UpSetData[, eval(name) := 0]
        upset.name = name
        UpSetData[ Identifiers %in% levels(as.factor(as.character(VarData[ Caller == "GATK", Var]))), eval(name) := 1]
      }
      # UpSetMetadata
      UpSetMetadata = data.table(sets = colnames(UpSetData))
      UpSetMetadata = UpSetMetadata[-1,]
      UpSetMetadata[, Lineage := "TopAssigned"]
      UpSetMetadata[, col := "gold1"]
    
      if(NoPangolinData == 1 & NoPangolinVarData == 0){
        UpSetMetadata[sets == PangolinLineage, Lineage := "Pangolin"]
        UpSetMetadata[sets == PangolinLineage, col := "red2"]
      }
      UpSetMetadata[ sets == upset.name, col := "blue3"]
      UpSetMetadata[ sets == upset.name, Lineage := "Sample"]
    
      if(NoPangolinVarData == 0){
        if (PangolinLineage %in% Counts.data$Lineage){
          UpSetMetadata[ sets == PangolinLineage & Lineage == "TopAssigned", col := "darkorange1"]
          UpSetMetadata[ sets == PangolinLineage & Lineage == "TopAssigned", Lineage := "Pangolin\nTopAssigned"]
        } else {
          UpSetMetadata[ sets == PangolinLineage, Lineage := "Pangolin"]
          UpSetMetadata[ sets == PangolinLineage, col := "red2"]
        }
      }
      Export.var = UpSetData[UpSetData[, rowSums(.SD), .SDcols = -1] == 1 & get(upset.name) == 1, ]
      fwrite(Export.var, file = paste0(ReportDir,"/",name,".PrivateVarVsTopUnambiguous.tab"), sep = "\t", quote = F, col.names = T)
    
      cols = c()
      for(lev in levels(as.factor(UpSetMetadata$Lineage))){
       col = UpSetMetadata[Lineage == lev, col][1]
        names(col) = lev
        cols = c(cols,col)
      }
    
      if(NoPangolinData == 1){
        UpsetPlot = upset(UpSetData, order.by = "freq",nintersects = NA, nsets = ncol(UpSetData)-1,
                        set.metadata = list(data = UpSetMetadata, plots = list(
                          list(type = "text", column = "Lineage", colors = cols, assign = nrow(UpSetMetadata)),
                          list(type = "matrix_rows", column = "Lineage", colors = cols,alpha = 0.5)
                        ))
        )
      } else {
        UpsetPlot = upset(UpSetData, order.by = "freq",nintersects = NA, nsets = ncol(UpSetData)-1,
                        set.metadata = list(data = UpSetMetadata, plots = list(
                          list(type = "text", column = "Lineage", colors = cols, assign = nrow(UpSetMetadata)),
                          list(type = "matrix_rows", column = "Lineage", colors = cols,alpha = 0.5)
                        ))
        )
      }
    }
  }
} 


########################
# Generate a ReportTable
# 
out = data.table(
  Sample = name,
  CoverageMean = Cov.mean,
  CoverageSd = Cov.sd,
  CoverageMedian = Cov.median,
  RefSitesQualCheck = RefSitesCheck,
  RefVarsQualCheck = RefVarsCheck,

  GatkCallsCount = GatkCount,
  GatkPassCallsCount = GatkPassCount, 
  GatkPassCallsPerc = GatkPassPerc, 
  GatkDPPassCount = GatkDPPassCount,
  GatkAFPass = GatkAFPassCount, 
  GatkZscorePassCount = GatkZPassCount,
  
  LoFreqCallsCount = LoFreqCount,
  LoFreqPassCallsCount = LoFreqPassCount,
  LoFreqPassCallsPerc = LoFreqPassPerc,
  LoFreqDPPassCount = LoFreqDPPassCount,
  LoFreqAFPassCount = LoFreqAFPassCount, 
  LoFreqZscorePassCount = LoFreqZPassCount,
  
  UnknownsVariantsCount = UnknownVariantsCount, 
  LineagePrivateVariantsCounts = LineagePrivateVariantsObserved,

  PangolinLineage = PangolinLineage,
  PangolinLineageScore = PangolinScore,
  
  FreyjaLineage = FreyjaLin,
  FreyjaLineageFrequency = FreyjaLinFreq,
  FreyjaVOC = FreyjaVOC,
  FreyjaVOCFrequency = FreyjaVOCFreq,
  
  LABELLineage = LABELLin,
  GenoFluLineage = GenoFluLin,
  
  VirstrainReadsBest = VirstrainBest,
  VirstrainContigBestSp = VirstrainContigBestSp, 
  VirstrainContigBestSt = VirstrainContigBestSt,

  CustomTopLineage = CustomTopLineage,
  SampleInLineageOutTotSample = SampleInLineageOutTotSample,
  SampleInLineageOutTotLineage = SampleInLineageOutTotLineage,
  CustomTopLineageScore = CustomTopLineageScore
)
   
if(!(skip_readSS)){
  out[, TopUnAmbiguousLineage := Stats.data[1, Lineage]]
  out[, TopUnAmbiguousReadsFreq := Stats.data[1, unambiguousReadsFreq]]
} else {
  out[, TopUnAmbiguousLineage := NA]
  out[, TopUnAmbiguousReadsFreq := NA]
}

fwrite(out, MainReport, sep = "\t", quote = F, col.names = T)

if(! (skip_plots)){
  pdf(file = PlotsReport, width = 15, height = 15)
  if(! skip_plotsReadSS){
    print(ReadSS_plot)
  }
  if(! (skip_plotsAF)){
    print(AF_plot)
  }
  if(file.exists(varDBpath)){
    print(Overl_plot)
  }
  dev.off()
  
  if( ! (skip_plotsUpSet | skip_readSS | GatkPassCount == 0 | (!(file.exists(varDBpath))) )){
    pdf(file = UpsetReport, width = 15, height = 15)
    print(UpsetPlot)
    dev.off()
  }
}  


# END
