#!/usr/bin/pyhon

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import coloredlogs, logging
import subprocess

def do_subprocess(cmd, condaEnv, envName):
    # define env
    env_path = os.path.join(condaEnv, envName)
    subprocess_env = os.environ.copy()
    subprocess_env['PATH'] = os.path.join(env_path, 'bin') + os.pathsep + subprocess_env['PATH']

    process = subprocess.Popen(cmd,
                               shell=True,
                               env = subprocess_env)
    process.wait()
    return process.returncode

# running bwa to remove host reads
def HostRemoval(condaBin, condaEnv, logDir, reads, mates, reference, threads, outDir, fastqDir, name):
    outPrefix = outDir + name
    logPrefix = logDir + name
    fastqPrefix = fastqDir + name
    stdOutFile = logPrefix + ".HostRemoval.log"
    stdErrFile = logPrefix + ".HostRemoval.stderr"
    out1 = fastqPrefix + ".cleaned.R1.fq.gz"
    out2 = fastqPrefix + ".cleaned.R2.fq.gz"
    bam = outPrefix + ".Host.bam"

    logger = logging.getLogger(__name__)
    logger.info('Host reads removal step: STARTED')

    threads = str(threads)

    index = reference  + ".pac"

    if(os.path.isfile(index)):
        Command1 = ""
    else:
        Command1 = f"bwa index {reference} 2>> {stdErrFile}"     
    Command1 += f"bwa mem -t {threads} {reference} {reads} {mates} 2>> {stdOutFile} | samtools view -uS - 2>> {stdErrFile} | samtools sort - > {bam} 2>> {stdErrFile}"
    Command2 = f"samtools index {bam} 2>> {stdErrFile}"
    Command3 = f"samtools fastq -f 4 -c 6 -@ {threads} -1 {out1} -2 {out2} -0 /dev/null -s /dev/null -n {bam} 2>> {stdErrFile}"
    Command4 = f"rm {outPrefix}.Host.ba*"

    for cmd in [Command1,Command2,Command3,Command4]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "bwa")
 
        if check != 0:
            logger.error('Host reads removal step: ERROR!')
            sys.exit('Exiting...')
 
    logger.info('Host reads removal step: COMPLETED!')
    
    return out1,out2

# running frejya
def freyja(condaBin,condaEnv,logDir,bam,outDir,name,wuhCor1Ref):
    
    outPrefix = outDir + name
    logPrefix = logDir + name
    outVCF = outPrefix + ".variants"
    outDepth = outPrefix + ".depth"
    outFile = outDir + "Freyja_report.txt"
    stdErrFile = logPrefix + ".freyja.stderr"
    stdOutFile = logPrefix + ".freyja.stdout"

    logger = logging.getLogger(__name__)
    logger.info("Freyja step: STARTED")

    Command1 = (
        f"freyja variants {bam} --variants {outVCF} --depths {outDepth} --ref {wuhCor1Ref} >> {stdOutFile} 2>> {stdErrFile}"
    )
    Command2 = (
        f"freyja demix {outVCF}.tsv {outDepth} --output {outFile} >> {stdOutFile} 2>> {stdErrFile}"
    )

    for cmd in [Command1,Command2]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "freyja")

        if check != 0:
            logger.error('Freyja: ERROR!')
            sys.exit('Exiting...')
    
    logger.info("Freyja step: COMPLETED!")
    
    return

def GenoFlu(condaBin, condaEnv, logDir, fasta, outDir, name):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".GenoFlu.stderr"
    stdOutFile = outPrefix + ".GenoFlu.out"

    logger = logging.getLogger(__name__)
    logger.info("GenoFlu step: STARTED")

    Command1 = (
        f"genoflu.py -f {fasta} > {stdOutFile} 2>> {stdErrFile}; rm *_stats.xlsx; rm *_stats.tsv"
    )

    logger.debug('Running: %s', Command1)
    check=do_subprocess(Command1, condaEnv, "genoflu")

    if check != 0:
        logger.error('GenoFlu: ERROR!')
        sys.exit('Exiting...')
    
    logger.info("GenoFlu step: COMPLETED!")

    return

def LABEL(condaBin, condaEnv, logDir, threads, fasta, outDir, name, LabelModule):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".LABEL.stderr"
    stdOutFile = logPrefix + ".LABEL.stdout"

    logger = logging.getLogger(__name__)
    logger.info("LABEL step: STARTED")

    Command1 = (
        f"/eos/jeodpp/data/projects/OMICS/leoniga/bin/LABEL -P {threads} -O {outDir} {fasta} {name}.LABEL {LabelModule} >> {stdOutFile} 2>> {stdErrFile}"
    )

    logger.debug('Running: %s', Command1)
    check=do_subprocess(Command1, condaEnv, "LABEL")

    if check != 0:
        logger.error('LABEL: ERROR!')
        sys.exit('Exiting...')
    
    logger.info("LABEL step: COMPLETED!")

    return

def split(condaBin, condaEnv, logDir, bam, name, threads, outDir):

    outPrefix = outDir + name
    logPrefix = logDir + name
    sam = outPrefix + ".sam"
    header = outPrefix + ".header.txt"
    split = outPrefix + ".split."
    split2 = outPrefix + ".split*"
    stdErrFile = logPrefix + ".split.stderr"
    stdOutFile = logPrefix + ".split.stdout"

    logger = logging.getLogger(__name__)
    logger.info("Split step: STARTED")

    Command1 = (
    f"samtools view -H {bam} > {header} && "
    f"samtools view {bam} > {sam} && "
    f"split -d -l $(echo `wc -l {sam} | cut -f 1 -d \" \"` / {threads} + 1 | bc) {sam} {split}"
    )
    Command2 = (
        f"for i in `ls {split2}`; do "
        f"cat {header} $i > $i.sam; "
        f"done && rm {outDir}*.split.??"
    )

    for cmd in [Command1,Command2]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "freyja")
        if check != 0:
            logger.error('Split: ERROR!')
            sys.exit('Exiting...')
    
    logger.info("Split step: COMPLETED!")
    
    return

def periscope(condaBin, condaEnv, logDir, script, bam, score, name, bed, primerBed, ampliconBed, tmpDir, threads, outDir):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".periscope.stderr"
    stdOutFile = logPrefix + ".periscope.stdout"
    bam2 = bam.replace("CovigatOff","Periscope")
    bam2 = bam2.replace("preprocessed.","")
    bai = bam.replace("bam","bai")
    bai2 = bam2.replace("bam","bai")
    bai2 = bai2.replace("preprocessed.","")

    logger = logging.getLogger(__name__)
    logger.info("Periscope step: STARTED")

    commands = [
        f"cp {bam} {bam2}",
        f"cp {bai} {bai2}",
        (
            f"python {script} --bam {bam2} --score-cutoff {score} --output-prefix {outPrefix} --sample {name} "
            f"--orf-bed {bed} --primer-bed {primerBed} --amplicon-bed {ampliconBed} --tmp {tmpDir} --threads {threads} "
            f"# 2>> {stdErrFile}"
        ),
        f"rm {bam2}",
        f"rm {bai2}",
        f"rm -rf {tmpDir}"
    ]

    for cmd in commands:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "periscope")
    
        if check != 0:
            logger.error('Periscope: ERROR!')
            sys.exit('Exiting...')
    
    logger.info("Periscope step: COMPLETED!")

    return

def varExprPeriscope(condaBin, condaEnv, script, bam, name, outDir, VcfPeriscope):

    outPrefix = outDir + name
    vcf = VcfPeriscope.replace(".gz","")

    logger = logging.getLogger(__name__)
    logger.info("Variant Expression Periscope step: STARTED")

    commands = [
        f"gunzip {VcfPeriscope}",
        f"sed -i -r 's/ +/_/g' {vcf}",
        f"python {script} --periscope-bam {bam} --output-prefix {outPrefix} --vcf {vcf} --sample {name}",
        f"rm {outPrefix}*.sam*",
        f"rm {outPrefix}*header*",
        f"rm {outPrefix}*periscope.bam"
    ]
    
    for cmd in commands:
        print(cmd)
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "periscope")
        if check != 0:
            logger.error('Variant Expression Periscope: ERROR!')
            sys.exit('Exiting...')
    
    logger.info("Variant Expression Periscope step: COMPLETED!")

    return

# Running Virstrain
def virstrain(condaBin, condaEnv, logDir, outDir, name, virstrainDBDir, virstrain_internalDB, **kwargs):
    
    outPrefix = outDir + name
    logPrefix = logDir + name

    logger = logging.getLogger(__name__)
    logger.info("Virstrain step: STARTED")

    if virstrain_internalDB:
        stdErrFile = logPrefix + ".virstrain1.stderr"
        stdOutFile = logPrefix + ".virstrain1.stdout"
        Command = f"virstrain -i {kwargs['reads']} -p {kwargs['mates']} -d {virstrainDBDir} -o {outDir} >> {stdOutFile} 2>> {stdErrFile}; "
    else:
        stdErrFile = logPrefix + ".virstrain2.stderr"
        stdOutFile = logPrefix + ".virstrain2.stdout"
        Command = f"virstrain_contig -i {kwargs['fasta']} -d {virstrainDBDir} -o {outDir} >> {stdOutFile} 2>> {stdErrFile}; "

    logger.debug('Running: %s ', Command)
    
    check=do_subprocess(Command, condaEnv, "virstrain.1.17")
    if check != 0:
        logger.error('Virstrain: ERROR!')
        sys.exit('Exiting...')
    else:
        logger.info("Virstrain step: COMPLETED!")
                        
    return 

# insilico sequencing
def iss(condaBin, condaEnv, logDir, inputFasta, outDir, name, model, nreads, threads):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".iss.stderr"

    reads = outPrefix + "_R1.fastq.gz"
    mates = outPrefix + "_R2.fastq.gz"

    logger = logging.getLogger(__name__)
    logger.info("InSilicoSeq step: STARTED")

    commands = [
        f"iss generate -g {inputFasta} --n_reads {nreads} --model {model} --cpus {threads} --output {outPrefix} 2>> {stdErrFile}",
        f"pigz -p {threads} -c {outPrefix}_R1.fastq > {reads}",
        f"pigz -p {threads} -c {outPrefix}_R2.fastq > {mates}",
        f"rm {outPrefix}_R1.fastq",
        f"rm {outPrefix}_R2.fastq"
    ]

    for cmd in commands:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "iss")
        if check != 0:
            logger.error('InSilicoSeq: ERROR!')
            sys.exit('Exiting...')
    
    logger.info("InSilicoSeq step: COMPLETED!")

    return reads, mates


# Clean and move files into the correct folders
def clean(wd):

    readSSDir = os.path.join(wd, "ReadStrainScanner")
    covDir = os.path.join(wd,"CovigatOff")
    pangoDir = os.path.join(covDir, "Pangolin")
    logDir = os.path.join(wd,"Logs")

    logger = logging.getLogger(__name__)
    logger.info("Cleaning step: STARTED")

    # Move ReadStrainScanner bam files to bam
    commands = [
  #   f"rm -rf {wd}/ref/",
  #   f"rm {wd}/Rplot.pdf"
    ]

    for cmd in commands:
        logger.debug('Running: %s', cmd)
        check=os.system(cmd)

        if check != 0:
            logger.warning('Cleaning: Some File was not moved or deleted. Check the Logs for further information')

            # logger.error('Cleaning: ERROR!')
            # sys.exit('Exiting...')
    
    logger.info("Cleaning step: COMPLETED!")

    return

# Call the VarStats.R file to generate distributions and coverage analyses
def varStats(condaBin, condaEnv, logDir, wd, outDir, name, VcfSnpLoFreq, VcfSnpCaller1, varStatsPath, varDB, minDP, percDP, refVar, refSites, minQD, minMQ, maxFS, minAF, zFilterThreshold):
    
    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".varStats.stderr"
    stdOutFile = logPrefix + ".varStats.stdout"

    logger = logging.getLogger(__name__)
    logger.info("VarStats step: STARTED")
   
    minDP = str(minDP)
    percDP = str(percDP)
    minMQ = str(minMQ)
    minQD = str(minQD)
    maxFS = str(maxFS)
    minAF = str(minAF)
    zFilterThreshold = str(zFilterThreshold)

    Command1 = (
        f"Rscript {varStatsPath} --wd {wd} -n {name} --varDB {varDB} --refVar {refVar} --vcf1 {VcfSnpCaller1} --vcf2 {VcfSnpLoFreq} "
        f"--refSites {refSites} --minDP {minDP} --percDP {percDP} --minQD {minQD} --minMQ {minMQ} --maxFS {maxFS} --minAF {minAF} --zFilterThreshold {zFilterThreshold} "
    )

    Command1 += f" > {stdOutFile} 2>> {stdErrFile}"
    
    for cmd in [Command1]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "R")
        if check != 0:
            logger.error('VarStats: ERROR!')
            sys.exit('Exiting...')
    
    logger.info("VarStats step: COMPLETED!")

    return

# Call the MakeReport.R file to generate plots and outputs
def makeReport(condaBin, condaEnv, logDir, wd, outDir, name, makeReportPath, varDB, skipPlots, skipReadSS, skipCovigatOff, skipUpset, minAF, refVar, refSites):

    outPrefix = outDir + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".makeReport.stderr"
    stdOutFile = logPrefix + ".makeReport.stdout"

    logger = logging.getLogger(__name__)
    logger.info("MakeReport step: STARTED")
   
    minAF = str(minAF)

    Command1 = (
        f"Rscript {makeReportPath} --wd {wd} -n {name} --varDB {varDB} --refVar {refVar} "
        f"--refSites {refSites} --minAF {minAF} "
    )

    if skipPlots:
        Command1 += " --skip_plots "
    if skipReadSS:
        Command1 += " --skip_readSS "
    if skipCovigatOff:
        Command1 += " --skip_covigatOff "
    if skipUpset:
        Command1 += " --skip_upSet "

    Command1 += f" > {stdOutFile} 2>> {stdErrFile}"
    
    for cmd in [Command1]:
        logger.debug('Running: %s', cmd)
        check=do_subprocess(cmd, condaEnv, "R")
        if check != 0:
            logger.error('MakeReport: ERROR!')
            # sys.exit('Exiting...')
    
    logger.info("MakeReport step: COMPLETED!")

    return
