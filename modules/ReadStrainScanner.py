#!/usr/bin/pyhon

# © European Union, 2023, Author: Gabriele Leoni
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import coloredlogs, logging
import subprocess

def do_subprocess(cmd, env):
    process = subprocess.Popen(cmd,
                               shell=True,
                               env = env)
    process.wait()
    return process.returncode

# ReadStrainScanner Function
def readSS(condaBin, condaEnv, logDir, reads, mates, memory, threads, consensus, outDir, name):

    outPrefix = outDir + name
    bamPrefix = outDir + "Bam/" + name
    statPrefix = outDir + "Report/" + name
    logPrefix = logDir + name
    stdErrFile = logPrefix + ".readSS.stderr"
    stdOutFile = logPrefix + ".readSS.stdout"

    logger = logging.getLogger(__name__)

    cmd1 = "bbmap.sh -Xmx%s threads=%s ref=%s perfectmode=T maxsites=1 out=%s.mapped.bam outu=%s.unmapped.bam scafstats=%s.scafstats bamscript=bs.sh in1=%s in2=%s 2> %s" \
        % (str(memory),str(threads),consensus, bamPrefix, bamPrefix, statPrefix, str(reads), str(mates), stdErrFile)
    cmd2 = "bash bs.sh >> %s 2>> %s"  % (stdOutFile, stdErrFile)
    cmd3 = "rm bs.sh"
    cmd4 = "rm *mapped.ba*"

    logger.debug('Running: %s ', cmd1)
    logger.info("ReadSS step: STARTED")

    #conda env path
    env_path = os.path.join(condaEnv, 'bbmap')
    subprocess_env = os.environ.copy()
    subprocess_env['PATH'] = os.path.join(env_path, 'bin') + os.pathsep + subprocess_env['PATH']


    check=do_subprocess(cmd1, subprocess_env)
    
    if check != 0:
        logger.error('ReadSS: ERROR!')
        sys.exit('Exiting...')
    else:
        check1=do_subprocess(cmd2, subprocess_env)
        check2=do_subprocess(cmd3, subprocess_env)
        check3=do_subprocess(cmd3, subprocess_env)

        logger.info("ReadSS step: COMPLETED!")

    return
